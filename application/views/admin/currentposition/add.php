
<body>
<div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>currentposition_info details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
        <form action="<?php echo site_url('admin/currentposition/addcurrentposition');?>" method="post">
        <div class="col-md-8 col-lg-10" id="customers_dv_form">
            <fieldset class="form-horizontal">
                <?php if($this->session->flashdata('msg')) : ?>
                     <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                <?php endif;?>

              

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="currentpositionname" class="control-label col-lg-3">Location</label>
                    <div class="col-lg-9"> 
                         <select name="location" id="" class="form-control">
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($location_datas as $location_data) { ?>
                                      <option value="<?php echo $location_data->id;?>"><?php echo $location_data->title;?></option>
                                 <?php  } ?>
  
                          </select>
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="currentpositionname" class="control-label col-lg-3">Bus</label>
                    <div class="col-lg-9">
                         <select name="bname" id="" class="form-control">
                             <option value="" selected="selected">Select Option</option>
                          <?php foreach ($bus_info_datas as $bus_info_data) { ?>
                                      <option value="<?php echo $bus_info_data->id;?>"><?php echo $bus_info_data->bus_name;?></option>
                                 <?php  } ?>
                             </select>

                    </div>
                </div>

            
 
                </fieldset>
            </div>
        </div>
                         <hr class="hidden-md hidden-lg">
                             <div class="col-md-4 col-lg-2">
                                <div class="btn-toolbar">
                                     <button type="submit" name="add" class="btn btn-success"> Save New</button>
                                </div>
                             </div>
                         </hr>
  

            </div> 
        </form>
        </div>
    </div>
 </div>   





