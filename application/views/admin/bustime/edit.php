<?php 


if(isset($_POST['submit'])){
   $bustime_type = $bustime_data[0]->b_type;
    $time = $bustime_data[0]->time;
    $btn = "Update";
    $btnName = 'update';
    $pageTitle = 'Edit';
  }else{
    if(isset($_POST['submit'])){
   
    $bustime_type = "";
    $time="";
    $btn = "Save";
    $btnName = 'add';
    $pageTitle = 'Add';
  }
  
}


?>



 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class ="container">
         <h1 >
        bustime
        <small>Edit Info</small>
        </h1>
        <ol class="breadcrumb">
        
           <li class="active">edit</li>
        </ol>
      </div>
    </section>
    <!-- Main content -->
   
    <div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>bustime details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
     
               <?php if($this->session->flashdata('msg')) : ?>
                         <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                 <?php endif;?>
                      <fieldset class="form-horizontal">
                          <?php if(isset($bustime_id)){ ?>
                               <form role="form" action="<?php echo site_url('admin/bustime/updatebustime/'.$bustime_id);?>" method="post">
                          <?php } 
                          else { ?>
                               <form role="form" action="<?php echo site_url('admin/bustime/addbustime/');?>" method="post">
                          <?php } ?>
  
                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

             

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">                               
                  <label for="bus_type" class="control-label col-lg-3">Bus type</label>   
                   <div class="col-lg-9">               
                        <select class="form-control" id="bus_type" name="b_type" required>                                                              
                                                    <option value="">Select</option>
                                                    <option value="ac">ac</option>
                                                    <option value="non ac">non ac</option>
                                                     <option value="ac business class">ac business class</option>
                                                                                                   
                         </select> 
                   </div>
                   </div>            
              
                 <div class="form-group ">
                        <label for="route_id" class="control-label col-lg-3">Time</label>
                        <div class="col-lg-9">
                            
                        <input maxlength="40" type="text" class="form-control" name="time" id="time" value="">
                     </div>
                </div>         
  </form>
             </form>
                </fieldset>
            </div>
       
    </div>
   </div>    
             <hr class="hidden-md hidden-lg">
                  <div class="col-md-4 col-lg-2">
                      <div class="box-footer">
                         <button type="submit" name="update" class="btn btn-primary">Update</button>
                      </div>
                  </div>
                         
   </div>
 </div>
     