<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       BUSTIME
        <small>Add Info</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("admin/dashboard");?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("admin/students");?>">Students</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Bus-TIME details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if($this->session->flashdata('msg')) : ?>
              <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
            <?php endif;?>

    <form action="<?php echo site_url('admin/bustime/addbustime');?>" method="post">
         
                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

   
                 <div class="form-group ">
                        <label for="route_id" class="control-label col-lg-3">Time</label>
                        <div class="col-lg-9">
                            
                        <input maxlength="40" type="text" class="form-control" name="time" id="time" value="">
                     </div>
                </div>         


         <div class="col-md-4 col-lg-2">
           <div class="btn-toolbar">
              <button type="submit" name="add" class="btn btn-success"> Save New</button>
            </div>
          </div>
                     
       </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
