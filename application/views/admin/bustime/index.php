<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
             
                <i class="fa fa-clock-o" style="color: #000;font-size: 35px;">
                   <span style="font-size:20px;margin-right: 10px">busTIME</span>
                  <a href="<?php echo site_url('admin/bustime/add');?>" class="btn btn-success">Add <i class="fa fa-plus"></i></a></i>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if($this->session->flashdata('msg')) : ?>
              <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
            <?php endif;?>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                 <tr>
                  <th>SL NO</th>
                   
          <th>time</th>
        <th>Action</th>
                  </tr>
                </thead>
               
                <tbody>
           <?php $i=1;foreach ($btime_data as $btime) :  ?>
                <tr>
                  <td><?php echo $i;?></td>
                 
                  <td><?php echo $btime->time;?></td>
                  
                   <td>
                    <a href="<?php echo site_url('admin/bustime/edit/'.$btime->id);?>"><i class="fa fa-pencil"></i></a>
                    &nbsp;&nbsp;
                    <a href="<?php echo site_url('admin/bustime/delete/'.$btime->id);?>" class='delete'><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
               <?php $i++; endforeach;?>
        </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    
  