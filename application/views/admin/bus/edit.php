<<?php 


if(isset($_POST['submit'])){
   $bus_name = $$bus_data[0]->bus_name;
    $bus_rnumber = $bus_data[0]->bus_rnumbr;
    $btn = "Update";
    $btnName = 'update';
    $pageTitle = 'Edit';
  }else{
    if(isset($_POST['submit'])){
    $bus_name = "";
   $bus_rnumber = "";
    $btn = "Save";
    $btnName = 'add';
    $pageTitle = 'Add';
  }
  
}


?>



  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class ="container">
         <h1 >
        businfo
        <small>Edit Info</small>
        </h1>
        <ol class="breadcrumb">
        
           <li class="active">edit</li>
        </ol>
      </div>
    </section>
    <!-- Main content -->
   
   
    <div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>bus_info details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
     

           
                 <?php if($this->session->flashdata('msg')) : ?>
                         <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                 <?php endif;?>
           <fieldset class="form-horizontal">
            <?php if(isset($bus_id)){ ?>
              <form role="form" action="<?php echo site_url('admin/bus/updatebus/'.$bus_id);?>" method="post">
              <?php } else { ?>
                <form role="form" action="<?php echo site_url('admin/bus/addbus/');?>" method="post">
              <?php } ?>


                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>


                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="busname" class="control-label col-lg-3">Busname</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="bus_name" id="busname" value="<?php echo $bus_data[0]->bus_name;?>"">
                    </div>
                </div>

                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="b_rnumber"
                     class="control-label col-lg-3">Bus regNumber</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="b_rnumber" id="b_rnumber" value="<?php echo $bus_data[0]->bus_rnumbr;?>"">
                    </div>
                </div>
                  
                
  
        
             <hr class="hidden-md hidden-lg">
                  <div class="col-md-4 col-lg-2">
                      <div class="box-footer">
                         <button type="submit" name="update" class="btn btn-primary">Update</button>
                      </div>
                  </div> 


                         
  </form>
             </form>
                </fieldset>
            </div>
       

</div>
</div>


                         
   </div>
 </div>
     