<<?php 


if(isset($_POST['submit'])){
   $bustype_route_name = $$bustype_data[0]->route_name;
   $bustype_title = $$bustype_data[0]->title;
    $bustype_seat = $bustype_data[0]->seat;
     $bustype_layout = $bustype_data[0]->layout;
      $bustype_snumber = $bustype_data[0]->snumber;
      $bustype_amount =  $bustype_data[0]->amount;
    $btn = "Update";
    $btnName = 'update';
    $pageTitle = 'Edit';
  }else{
    if(isset($_POST['submit'])){
       $bustype_route_name ="";
    $bustype_title = "";
   $bustype_seat = "";
   $bustype_layout = "";
   $bustype_snumber = "";
   $bustype_amount ="";
    $btn = "Save";
    $btnName = 'add';
    $pageTitle = 'Add';
  }
  
}


?>



  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class ="container">
         <h1 >
        bustype
        <small>Edit Info</small>
        </h1>
        <ol class="breadcrumb">
        
           <li class="active">edit</li>
        </ol>
      </div>
    </section>
    <!-- Main content -->
   
   
    <div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>bustype details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
     

           
                 <?php if($this->session->flashdata('msg')) : ?>
                         <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                 <?php endif;?>
           <fieldset class="form-horizontal">
            <?php if(isset($bustype_id)){ ?>
              <form role="form" action="<?php echo site_url('admin/bustype/updatebustype/'.$bustype_id);?>" method="post">
              <?php } else { ?>
                <form role="form" action="<?php echo site_url('admin/bustype/addbustype/');?>" method="post">
              <?php } ?>


                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group ">
                        <label for="bfrom" class="control-label col-lg-3">Route Name*</label>
                        <div class="col-lg-9">
                            <select name="route_name" id="route_name" class="form-control">
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($route_datas as $route_data) { ?>
                                      <option value="<?php echo $route_data->id;?>"><?php echo $route_data->route_name;?></option>
                                 <?php  } ?>
  
                          </select>
                     </div>
                  </div>


  <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="bustypetypename" class="control-label col-lg-3">bustypeTitle</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="title" id="bustypetypename" value="">
                    </div>
                </div>

                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="seat"
                     class="control-label col-lg-3">Seat</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="seat" id="seat" value="">
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="layout"
                     class="control-label col-lg-3">LAYOUT</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="layout" id="layout" value="">
                    </div>
                </div>
                  
                     <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="snumber"
                     class="control-label col-lg-3">Seatnumber</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="snumber" id="snumber" value="">
                    </div>
                </div>
                  
                        <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="amount"
                     class="control-label col-lg-3">Amount</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="amount" id="snumber" value="">
                    </div>
                </div>           


 
        
             <hr class="hidden-md hidden-lg">
                  <div class="col-md-4 col-lg-2">
                      <div class="box-footer">
                         <button type="submit" name="update" class="btn btn-primary">Update</button>
                      </div>
                  </div> 


                         
  </form>
             </form>
                </fieldset>
            </div>
       

</div>
</div>


                         
   </div>
 </div>
     