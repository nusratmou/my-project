




  <!-- Content Wrapper. Contains page content -->
<div class="container">

        <div id="right">
                    <div class="content-middle" id="content">
                            
        <div class="b10">

            
            <div class="box-header">
                <?php if(($this->session->userdata('logged_in_type') =='admin')){ ?>
              <a href="<?php echo site_url('admin/bustype/add');?>" class="btn btn-success">Add <i class="fa fa-plus"></i></a>
                  <?php } ?>
                 <input type="text" name="q" class="pj-form-field pj-form-field-search w150" placeholder="Search" />
         
            </div>
            </div>
            
        </div>

        

  <?php if($this->session->flashdata('msg')) : ?>
              <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
            <?php endif;?>
              
    <table cellpadding="0" cellspacing="0" class="pj-table" style="width: 100%;"><thead>
    <tr>
        <th>
            <input type="checkbox" class="pj-table-toggle-rows">
        </th>

        <th>
            <div class="pj-table-sort-label">Route name</div>
           
        </th>
        <th>
            <div class="pj-table-sort-label">TITLE</div>
        </th>
        <th>
            <div class="pj-table-sort-label">Seat</div>
           
        </th>
      <th>
            <div class="pj-table-sort-label">Layout</div>
           
        </th>
         <th>
            <div class="pj-table-sort-label">seatNumber</div>
           
        </th>
         <th>
            <div class="pj-table-sort-label">time</div>
           
        </th>
         <th>
            <div class="pj-table-sort-label">amount</div>
           
        </th>
        <th>
            <div class="pj-table-sort-label">Action</div>
            
        </th>
      
    </tr>
</thead>

 <tbody>
  <?php $i=1;foreach ($bustype_data as $bustype) :  ?>
                <tr>
                  <tr>
                  <td><?php echo $i;?></td>
                    <td><?php echo $bustype->route_name;?></td>
                  <td><?php echo $bustype->title;?></td>
                 <td><?php echo $bustype->seat;?></td>
                 <td><?php echo $bustype->layout;?></td>
                 <td><?php echo $bustype->snumber;?></td>
                 <td><?php echo $bustype->bus_tym;?></td>
                    <td><?php echo $bustype->amount;?></td>
                 
     
                  <td>
                    <a href="<?php echo site_url('admin/bustype/edit/'.$bustype->id);?>"><i class="fa fa-pencil"></i></a>
                    &nbsp;&nbsp;
                    <a href="<?php echo site_url('admin/bustype/delete/'.$bustype->id);?>" class='delete'><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
               <?php $i++; endforeach;?>
               
 </tbody>


</table>
</div>
</div>
              
            

                    <div class="content-bottom"></div>
                </div> <!-- content -->
                <div class="clear_both"></div>
         
        </div> <!-- container -->

    <!-- /.content -->
  </div>
       
