

<body>
<div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>Route details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
        <form action="<?php echo site_url('admin/route/addroute');?>" method="post">
        <div class="col-md-8 col-lg-10" id="customers_dv_form">
            <fieldset class="form-horizontal">
                <?php if($this->session->flashdata('msg')) : ?>
                     <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                <?php endif;?>
                

                <div class="form-group ">
                        <label for="bfrom" class="control-label col-lg-3">From *</label>
                        <div class="col-lg-9">
                            <select name="bfrom" id="bfrom" class="form-control">
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($location_datas as $location_data) { ?>
                                      <option value="<?php echo $location_data->id;?>"><?php echo $location_data->title;?></option>
                                 <?php  } ?>
  
                          </select>
                     </div>
                  </div>

                  <div class="form-group ">
                        <label for="bto" class="control-label col-lg-3">To *</label>
                        <div class="col-lg-9">
                            <select name="bto" id="bto" class="form-control">
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($location_datas as $location_data) { ?>
                                      <option value="<?php echo $location_data->id;?>"><?php echo $location_data->title;?></option>
                                 <?php  } ?>
 
                          </select>
                     </div>
                 </div>

                </fieldset>
            </div>
        </div>
                         <hr class="hidden-md hidden-lg">
                             <div class="col-md-4 col-lg-2">
                                <div class="btn-toolbar">
                                     <button type="submit" name="add" class="btn btn-success"> Save New</button>
                                </div>
                             </div>
                         </hr>
  

            </div> 
        </form>
        </div>
    </div>
 </div>   

