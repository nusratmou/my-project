<?php 


if(isset($_POST['submit'])){
   $seat_name = $seat_data[0]->b_name;
    $seat_type = $seat_data[0]->b_stype;
    $btn = "Update";
    $btnName = 'update';
    $pageTitle = 'Edit';
  }else{
    if(isset($_POST['submit'])){
    $seat_name = "";
    $seat_type = "";
    $btn = "Save";
    $btnName = 'add';
    $pageTitle = 'Add';
  }
  
}


?>

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class ="container">
         <h1 >
        seat
        <small>Edit Info</small>
        </h1>
        <ol class="breadcrumb">
        
           <li class="active">edit</li>
        </ol>
      </div>
    </section>
    <!-- Main content -->
   
    <div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>seat details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
     
               <?php if($this->session->flashdata('msg')) : ?>
                         <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                 <?php endif;?>
                      <fieldset class="form-horizontal">
                          <?php if(isset($seat_id)){ ?>
                               <form role="form" action="<?php echo site_url('admin/seat/updateseat/'.$seat_id);?>" method="post">
                          <?php } 
                          else { ?>
                               <form role="form" action="<?php echo site_url('admin/seat/addseat/');?>" method="post">
                          <?php } ?>

               <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="fullname" class="control-label col-lg-3">Fullname</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="fullname" id="fullname" value="" required>
                    </div>
                </div>

                 
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">                               
                          <label for="phone" class="control-label col-lg-3">SEAT TYPE</label>   
                             <div class="col-lg-9">               
                                <select class="form-control" id="seat_type" name="seattype" required>                                                              
                                                    <option value="">Seat</option>
                                                    <option value="1"  >sleeper</option>
                                                    <option value="2"  >seat&&sleeper</option>
                                  </select> 
                            </div>
                 </div>   
  </form>
             </form>
                </fieldset>
            </div>
       
    </div>
   </div>    
             <hr class="hidden-md hidden-lg">
                  <div class="col-md-4 col-lg-2">
                      <div class="box-footer">
                         <button type="submit" name="update" class="btn btn-primary">Update</button>
                      </div>
                  </div>
                         
   </div>
 </div>
     
      