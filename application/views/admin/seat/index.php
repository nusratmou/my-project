



  <!-- Content Wrapper. Contains page content -->
 <div class="container">

    <div id="right">
                  
        <div class="content-middle" id="content">
                            
        <div class="b10">
            <div class="box-header"> 
                <?php if(($this->session->userdata('logged_in_type') =='admin')){ ?>
                 <a href="<?php echo site_url('admin/seat/add');?>" class="btn btn-success">Add <i class="fa fa-plus"></i></a>
                   <?php } ?>
                 <input type="text" name="q" class="pj-form-field pj-form-field-search w150" placeholder="Search" />
            </div>
        </div>

            <?php if($this->session->flashdata('msg')) : ?>
                 <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
            <?php endif;?>
              
    
   <table cellpadding="0" cellspacing="0" class="pj-table" style="width: 100%;">
    <thead>
    <tr>
        <th>
            <input type="checkbox" class="pj-table-toggle-rows">
        </th>
        <th>
            <div class="pj-table-sort-label">Name</div>
        </th>
        
        <th>
            <div class="pj-table-sort-label">Seat type</div>
           
        </th>
        <th>
            <div class="pj-table-sort-label">Action</div>
            
        </th>
       
    </tr>
</thead>
       
       <tbody>
           <?php $i=1;foreach ($seat_data as $seat) :  ?>
                <tr>
                  <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $seat->b_name;?></td>
                  <td><?php echo $seat->b_stype;?></td>
                  
                   <td>
                    <a href="<?php echo site_url('admin/seat/edit/'.$seat->id);?>"><i class="fa fa-pencil"></i></a>
                    &nbsp;&nbsp;
                    <a href="<?php echo site_url('admin/seat/delete/'.$seat->id);?>" class='delete'><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
               <?php $i++; endforeach;?>
               
 </tbody>


</table>
</div>


                    <div class="content-bottom"></div>
                </div> <!-- content -->
                <div class="clear_both"></div>
            </div> <!-- middle -->
        
        </div> <!-- container -->

    <!-- /.content -->
  </div>
        </div>
  