<?php 


if(isset($_POST['submit'])){
   $agent_name =  $_POST["$agent_data[0]->b_name;"];
    $agent_email = $agent_data[0]->b_email;
    $agent_pass = $agent_data[0]->b_pass;
    $agent_phone = $agent_data[0]->phone;
    $agent_cname = $agent_data[0]->b_cname;
    $agent_address = $agent_data[0]->b_address;
    $agent_city = $agent_data[0]->b_city;
    $btn = "Update";
    $btnName = 'update';
    $pageTitle = 'Edit';
  }else{
    if(isset($_POST['submit'])){
    $agent_name = "";
    $agent_email = "";
    $agent_pass = "";
    $agent_phone = "";
     $agent_cname = "";
    $agent_address = "";
    $agent_city = "";
    $btn = "Save";
    $btnName = 'add';
    $pageTitle = 'Add';
  }
  
}


?>



  


  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 >
        agent
        <small>Edit Info</small>
      </h1>
      <ol class="breadcrumb">
        
        <li class="active">edit</li>
      </ol>
    </section>

    <!-- Main content -->
   
    <div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>Customer details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
     

           
                 <?php if($this->session->flashdata('msg')) : ?>
                         <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                 <?php endif;?>
           <fieldset class="form-horizontal">
            <?php if(isset($agent_id)){ ?>
              <form role="form" action="<?php echo site_url('admin/agent/updateagent/'.$agent_id);?>" method="post">
              <?php } else { ?>
                <form role="form" action="<?php echo site_url('admin/agent/addagent/');?>" method="post">
              <?php } ?>

                <div class="box-body">
                    <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="fullname" class="control-label col-lg-3">Fullname</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="fullname" id="fullname" value="<?php echo $agent_data[0]->b_name;?>">
                    </div>
                </div>

                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="email" class="control-label col-lg-3">Email</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="email" class="form-control" name="email" id="email" value="<?php echo $agent_data[0]->b_email;?>">
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="password" class="control-label col-lg-3">Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="password" id="password" value="<?php echo $agent_data[0]->b_name;?>">
                    </div>
                </div>
                
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="Confirm password" class="control-label col-lg-3">Confirm Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="password" id="password" value="">
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Phone</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="phone" id="phone" value="<?php echo$agent_data[0]->phone;?>">
                    </div>
                </div>
               <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Company name</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="cname" id="cname" value="<?php echo $agent_data[0]->b_cname;?>">
                    </div>
                </div>
                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Address</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="address" id="Address" value="<?php echo $agent_data[0]->b_address;?>">
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">City</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="city" id="city" value="<?php echo $agent_data[0]->b_city;?>">
                    </div>
                </fieldset>
            </div>
       
    </div>
   </div>    
             <hr class="hidden-md hidden-lg">
                             <div class="col-md-4 col-lg-2">
                               <div class="box-footer">
                  <button type="submit" name="update" class="btn btn-primary">Update</button>
                </div>
                             </div>
                         </hr>
   </div>
</form>      

      
            </div> 
    </div>
      