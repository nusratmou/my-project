
<body>
<div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>Customer details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
       <!-- form inputs -->
        <form action="<?php echo site_url('admin/agent/addagent');?>" method="post">
        <div class="col-md-8 col-lg-10" id="customers_dv_form">
            <fieldset class="form-horizontal">
                <?php if($this->session->flashdata('msg')) : ?>
                     <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                <?php endif;?>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="fullname" class="control-label col-lg-3">Fullname</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="fullname" id="fullname" value="">
                    </div>
                </div>

                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="email" class="control-label col-lg-3">Email</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="email" class="form-control" name="email" id="email" value="">
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="password" class="control-label col-lg-3">Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="password" id="password" value="">
                    </div>
                </div>
                
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="Confirm password" class="control-label col-lg-3">Confirm Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="password" id="password" value="">
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Phone</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="phone" id="phone" value="">
                    </div>
                </div>
               <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Company name</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="cname" id="cname" value="">
                    </div>
                </div>
                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="address" class="control-label col-lg-3">Address</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="address" id="address" value="">
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">City</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="city" id="city" value="">
                    </div>
                </div>
              </fieldset>
            </div>
        </div>
                         <hr class="hidden-md hidden-lg">
                             <div class="col-md-4 col-lg-2">
                                <div class="btn-toolbar">
                                     <button type="submit" name="add" class="btn btn-success"> Save New</button>
                                </div>
                             </div>
                         </hr>
  

            </div> 
        </form>
        </div>
    </div>
 </div>   
