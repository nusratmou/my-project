



  <!-- Content Wrapper. Contains page content -->
<div class="container">

        <div id="right">
                    <div class="content-middle" id="content">
                            
        <div class="b10">

            
            <div class="box-header">
              
              <a href="<?php echo site_url('admin/agent/add');?>" class="btn btn-success">Add <i class="fa fa-plus"></i></a>
            
                 <input type="text" name="q" class="pj-form-field pj-form-field-search w150" placeholder="Search" />
         
            </div>
            </div>
            
        </div>
     

  <?php if($this->session->flashdata('msg')) : ?>
              <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
            <?php endif;?>
        

   <table cellpadding="0" cellspacing="0" class="pj-table" style="width: 100%;"><thead>
    <tr>
        <th>
            <input type="checkbox" class="pj-table-toggle-rows">
        </th>
        <th>
            <div class="pj-table-sort-label">Full Name</div>
        </th>
        <th>
            <div class="pj-table-sort-label">Email</div>
           
        </th>
        <th>
            <div class="pj-table-sort-label">password</div>
        </th>
        <th>
            <div class="pj-table-sort-label">Confirm password</div>
           
        </th>
        <th>
            <div class="pj-table-sort-label">phone</div>
            
        </th>
         <th>
            <div class="pj-table-sort-label">Company name</div>
            
        </th>
         <th>
            <div class="pj-table-sort-label">Address</div>
            
        </th>
         <th>
            <div class="pj-table-sort-label">City</div>
            
        </th>
         <th>
            <div class="pj-table-sort-label">action</div>
            
        </th>
     
</thead>

 <tbody>
  <?php $i=1;foreach ($agent_data as $agent) :  ?>
                <tr>
                  <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $agent->b_name;?></td>
                  <td><?php echo $agent->b_email;?></td>
                  <td><?php echo $agent->b_pass;?></td>
                  <td><?php echo $agent->b_cpass;?></td>
                  <td><?php echo $agent->phone;?></td>
                    <td><?php echo $agent->b_cname;?></td>
                  <td><?php echo $agent->b_address;?></td>
                  <td><?php echo $agent->b_city;?></td>
            
                 <!--- <button type="button" class="btn btn-info btn-xs">Edit</button>
                  <button type="button" class="btn btn-success btn-xs">Update</button>
                  <a href="<?php echo site_url('admin/agent/delete/'.$agent->id);?>" <button type="button" <a href=" class="btn btn-danger btn-xs">Delete</button></a>-->
       
                  <td>
                    <a href="<?php echo site_url('admin/agent/edit/'.$agent->id);?>"><i class="fa fa-pencil"></i></a>
                    &nbsp;&nbsp;
                    <a href="<?php echo site_url('admin/agent/delete/'.$agent->id);?>" class='delete'><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
               <?php $i++; endforeach;?>
               
 </tbody>


</table>
</div>
</div>
              
            

                    <div class="content-bottom"></div>
                </div> <!-- content -->
                <div class="clear_both"></div>
         
        </div> <!-- container -->

    <!-- /.content -->
  </div>
       
