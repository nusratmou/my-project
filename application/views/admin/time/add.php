

<body>
<div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>Timing</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
       <form action="<?php echo site_url('admin/time/addtime');?>" method="post">
        <div class="col-md-8 col-lg-10" id="customers_dv_form">
            <fieldset class="form-horizontal">
                <?php if($this->session->flashdata('msg')) : ?>
                     <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                <?php endif;?>
               
                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

               

                
                <div class="form-group ">
                        <label for="bfrom" class="control-label col-lg-3">Route Name*</label>
                        <div class="col-lg-9">
                            <select name="route_name" id="route_name" class="form-control">
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($route_datas as $route_data) { ?>
                                      <option value="<?php echo $route_data->id;?>"><?php echo $route_data->route_name;?></option>
                                 <?php  } ?>
  
                          </select>
                     </div>
                  </div>

                  <div class="form-group ">
                        <label for="bto" class="control-label col-lg-3">title *</label>
                        <div class="col-lg-9">
                            <select name="title" id="bto" class="form-control">
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($bus_type_datas as $bus_type_data) { ?>
                                      <option value="<?php echo $bus_type_data->id;?>"><?php echo $bus_type_data->title;?></option>
                                 <?php  } ?>
 
                          </select>
                     </div>
                 </div>


                 <div class="form-group ">
                        <label for="route_id" class="control-label col-lg-3">Time</label>
                        <div class="col-lg-9">
                         <select name="time" id="bto" class="form-control">
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($btime_datas as $btime_data) { ?>
                                      <option value="<?php echo $btime_data->id;?>"><?php echo $btime_data->time;?></option>
                                 <?php  } ?>
 
                          </select>
                     </div>
                </div>         





                </fieldset>
            </div>
        </div>
                         <hr class="hidden-md hidden-lg">
                             <div class="col-md-4 col-lg-2">
                                <div class="btn-toolbar">
                                     <button type="submit" name="add" class="btn btn-success"> Save New</button>
                                </div>
                             </div>
                         </hr>
  

            </div> 
        </form>
        </div>
    </div>
 </div>   




