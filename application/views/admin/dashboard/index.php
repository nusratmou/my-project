   

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="small-box bg-aqua" style="padding: 30px;background-color: #dc1432 !important;">
            <div class="inner">
             
                                <h3>3</h3>
                                  <p>CUSTOMERS</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
               
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
         
          <div class="small-box bg-aqua" style="padding: 30px;    background-color: #1c8832 !important;">
            <div class="inner">
             
                                <h3>3</h3>
                                  <p>BOOKINGS</p>
            </div>
            <div class="icon">
                <i class="fa fa-money"></i>
               
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
         
          <div class="small-box bg-aqua" style="padding: 30px;    background-color: #c5c518 !important;">
            <div class="inner">
             
                                <h3>3</h3>
                                  <p>CALENDERS</p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar"></i>
               
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
         
          <div class="small-box bg-aqua" style="padding: 30px;">
            <div class="inner">
             
                                <h3>3</h3>
                                  <p>ADMIN</p>
            </div>
            <div class="icon">
                <i class="fa fa-user"></i>
               
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->


           <div class="col-lg-3 col-xs-6">
         
          <div class="small-box bg-aqua" style="padding: 30px;background-color: #7e3484 !important;">
            <div class="inner">
             
                                <h3>3</h3>
                                  <p>LOCATION</p>
            </div>
            <div class="icon">
                <i class="fa fa-location-arrow"></i>
               
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


        <!-- ./col -->
       <div class="col-lg-3 col-xs-6">
         
          <div class="small-box bg-aqua" style="padding: 30px;">
            <div class="inner">
             
                                <h3>3</h3>
                                  <p>BUS TYPE</p>
            </div>
            <div class="icon">
                <i class="fa fa-bus"></i>
               
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
       <div class="col-lg-3 col-xs-6">
         
          <div class="small-box bg-aqua" style="padding: 30px;  background-color: #1c8832 !important;">
            <div class="inner">
             
                                <h3>3</h3>
                                  <p>timing</p>
            </div>
            <div class="icon">
                <i class="fa fa-clock-o"></i>
               
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
           <div class="col-lg-3 col-xs-6">
         
          <div class="small-box bg-aqua" style="padding: 30px;background-color: #dc1432 !important;">
            <div class="inner">
             
                                <h3>3</h3>
                                  <p>route</p>
            </div>
            <div class="icon">
                <i class="fa fa-road"></i>
               
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
          
          </div>
          <!-- /.nav-tabs-custom -->

       
          <!-- /.box (chat box) -->

          <!-- TO DO List -->
        
          <!-- /.box -->

          <!-- quick email widget -->
       

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- Map box -->
        
          <!-- /.box -->

          <!-- solid sales graph -->
      
          <!-- /.box -->

          <!-- Calendar -->
          <div class="box box-solid bg-green-gradient">
         
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-6">
                  <!-- Progress bars -->
                
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 90%;"></div>
                  </div>

                  
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 70%;"></div>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                  <div class="clearfix">
                    <span class="pull-left">Task #3</span>
                    <small class="pull-right">60%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 60%;"></div>
                  </div>

                  <div class="clearfix">
                    <span class="pull-left">Task #4</span>
                    <small class="pull-right">40%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 40%;"></div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.box -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 
   