

   <!-- Sidebar user panel >
      <ul class="user-panel">
        <li class="pull-left image">
          <img src="<?php echo $resource_dir;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </li>
        <li class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </li>
      </ul-->
      
            <!-- Sidebar user panel >
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $resource_dir;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div---->
      <nav class="navbar-default navbar-side" role="navigation">
 <div class="sidebar-collapse">
           <ul class="nav">


                    <li>
                        <a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                
                       <li>
                            <a href="#collapse-post" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-users"></i> <span class="c" aria-hidden="true">Customers</span>
                         
                            </a>
                            <ul class="collapse collapseable " id="collapse-post" style="list-style:none;">
                                 <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>

                                <li><a href="http://localhost/Project/admin/user/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                              <?php endif;?>
                                <li><a href="http://localhost/Project/admin/user"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                               
                            </ul>
                        </li>


                      <li>
                            <a href="#collapse-book" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-bus"></i> <span class="c" aria-hidden="true">Booking</span>
                               
                             
                            </a>
                           <ul class="collapse collapseable " id="collapse-book" style="list-style:none;">
                                
                                 <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>

                                <li><a href="http://localhost/Project/admin/booking/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                                <?php endif;?>
                                <li><a href="http://localhost/Project/admin/booking"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                                
                            </ul>
                        </li>
                    
                    
                         <li>
                            <a href="#collapse-bus" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-bus"></i> <span class="c" aria-hidden="true">Bus_info</span>
                               
                            
                            </a>
                           <ul class="collapse collapseable " id="collapse-bus" style="list-style:none;">

                               <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>
                                
                                <li><a href="http://localhost/Project/admin/bus/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                               <?php endif;?>
                                <li><a href="http://localhost/Project/admin/bus"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                               
                            </ul>
                        </li>
                    
                        <li>
                            <a href="#collapse-bustype" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-bus"></i> <span class="c" aria-hidden="true">Bustype</span>
                               
                            
                            </a>
                           <ul class="collapse collapseable " id="collapse-bustype" style="list-style:none;">

                               <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>
                                
                                <li><a href="http://localhost/Project/admin/bustype/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                               <?php endif;?>
                                <li><a href="http://localhost/Project/admin/bustype"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                               
                            </ul>
                        </li>


                        <li>
                            <a href="#collapse-time" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-clock-o"></i> <span class="c" aria-hidden="true">Timing</span>
                               
                            
                            </a>
                           <ul class="collapse collapseable " id="collapse-time" style="list-style:none;">

                               <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>
                                
                                <li><a href="http://localhost/Project/admin/time/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                               <?php endif;?>
                                <li><a href="http://localhost/Project/admin/time"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                               
                            </ul>
                        </li>
                    
                       
                        <li>
                            <a href="#collapse-btime" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-clock-o"></i> <span class="c" aria-hidden="true">Bus Timing</span>
                            </a>

                           <ul class="collapse collapseable " id="collapse-btime" style="list-style:none;">

                               <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>
                                
                                <li><a href="http://localhost/Project/admin/bustime/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                               <?php endif;?>
                                <li><a href="http://localhost/Project/admin/bustime"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                               
                            </ul>
                        </li>





                        
                         <li>
                            <a href="#collapse-location" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-location-arrow"></i> <span class="c" aria-hidden="true">Location</span>
                               
                              
                            </a>
                           <ul class="collapse collapseable " id="collapse-location" style="list-style:none;">
                                <li><a href="http://localhost/Project/admin/locatn/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                                
                                <li><a href="http://localhost/Project/admin/locatn"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>

                            </ul>
                        </li>


                         <li>
                            <a href="#collapse-cposition" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-map-marker"></i> <span class="c" aria-hidden="true">Current-position</span>
                               
                               
                            </a>
                           <ul class="collapse collapseable " id="collapse-cposition" style="list-style:none;"> 

                              <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>

                                <li><a href="http://localhost/Project/admin/currentposition/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                                 <?php endif;?>
                                <li><a href="http://localhost/Project/admin/currentposition"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                                
                            </ul>
                        </li>

                        <li>
                            <a href="#collapse-route" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-road"></i> <span class="c" aria-hidden="true">Routes</span>
                               
                               
                            </a>
                           <ul class="collapse collapseable " id="collapse-route" style="list-style:none;"> 

                              <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>

                                <li><a href="http://localhost/Project/admin/route/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                                 <?php endif;?>
                                <li><a href="http://localhost/Project/admin/route"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                                
                            </ul>
                        </li>

                         <li>
                            <a href="#collapse-admin" data-toggle="collapse" aria-hidden="collapse-post">
                               <i class="fa fa-user"></i> <span class="c" aria-hidden="true">Admins</span>
                               
                               
                            </a>
                           <ul class="collapse collapseable " id="collapse-admin" style="list-style:none;"> 

                              <?php if(($this->session->userdata('logged_in_type') =='admin')): ?>

                                <li><a href="http://localhost/Project/admin/admins/add"><i class="fa fa-plus" style="margin-right: 10px;"></i>Add New</a></li>
                                 <?php endif;?>
                                <li><a href="http://localhost/Project/admin/admins"><i class="fa fa-user"style="margin-right: 10px;"></i>View Detail</a></li>
                                
                            </ul>
                        </li>
                                         
                </ul>

            </div>

        </nav>   
