<!DOCTYPE html>
<html lang="en">
<head>
    <!--=== meta ===-->
    <meta charset="utf-8"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Page - Title</title>
   
     <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/n.css?v=1.1"">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>
<section id="wrapper">


  <nav class="navbar navbar-default top-navbar" role="navigation">
      
          <div class="container">
        
                 <div class="navbar-header">
            
                     <a class="navbar-brand" href="index.html"> Online bus</a>
        
                 </div>
                <ul class="nav navbar-top navbar-right">
                    <li class="dropdown">
                     <a href="<?php echo site_url('admin/login/do_logout');?>" class="btn  "><i class="fa fa-cog"></i> <strong>Logout</strong></a>
 
                </li>
                <!-- /.dropdown -->
            </ul>
        </nav>

        