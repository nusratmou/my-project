<?php 


if(isset($_POST['submit'])){
   $user_name =  $user_data[0]->b_name;
    $user_email = $user_data[0]->b_email;
    $user_pass = $user_data[0]->b_pass;
    $user_phone = $user_data[0]->phone;
    $btn = "Update";
    $btnName = 'update';
    $pageTitle = 'Edit';
  }else{
    if(isset($_POST['submit'])){
    $user_name = "";
    $user_email = "";
    $user_pass = "";
    $user_phone = "";
    $btn = "Save";
    $btnName = 'add';
    $pageTitle = 'Add';
  }
  
}

?>

  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class ="container">
         <h1 >
        user
        <small>Edit Info</small>
        </h1>
        <ol class="breadcrumb">
        
           <li class="active">edit</li>
        </ol>
      </div>
    </section>
    <!-- Main content -->
   
    <div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>Customer details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
       
                 <?php if($this->session->flashdata('msg')) : ?>
                         <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                 <?php endif;?>
           <fieldset class="form-horizontal">
            <?php if(isset($user_id)){ ?>
              <form role="form" action="<?php echo site_url('admin/user/updateuser/'.$user_id);?>" method="post">
              <?php } else { ?>
                <form role="form" action="<?php echo site_url('admin/user/adduser/');?>" method="post">
              <?php } ?>


                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="fullname" class="control-label col-lg-3">Fullname</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="fullname" id="fullname"
                         value="<?php echo $user_data[0]->b_name;?>" required >
                    </div>
                </div>

                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="email" class="control-label col-lg-3">Email</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="email" class="form-control" name="email" id="email" value="<?php echo $user_data[0]->b_email;?>" required >
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="password" class="control-label col-lg-3">Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="password" id="password" value="">
                    </div>
                </div>
                
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="con_password" class="control-label col-lg-3">Confirm Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="con_password" id="con_password" value="">
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Phone</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="phone" id="phone" value="<?php echo $user_data[0]->phone;?>">
                    </div>
                </div>

               </form>
             </form>
                </fieldset>
            </div>
       
    </div>
   </div>    
             <hr class="hidden-md hidden-lg">
                  <div class="col-md-4 col-lg-2">
                      <div class="box-footer">
                         <button type="submit" name="update" class="btn btn-primary">Update</button>
                      </div>
                  </div>
                         
   </div>
 </div>
     

      