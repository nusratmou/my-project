<?php 


if(isset($_POST['submit'])){
   $booking_name =  $booking_data[0]->b_name;
    $booking_email = $booking_data[0]->b_email;
    $booking_pass = $booking_data[0]->b_pass;
    $booking_route = $booking_data[0]->b_route;
    $booking_seat = $booking_data[0]->b_seat;
    $booking_amount = $booking_data[0]->b_amount;
    $booking_bustype = $booking_data[0]->b_bustype;
    $booking_date = $booking_data[0]->date;
    $btn = "Update";
    $btnName = 'update';
    $pageTitle = 'Edit';
  }else{
    if(isset($_POST['submit'])){
    $booking_name = "";
    $booking_email = "";
    $booking_pass = "";
    $booking_route = "";
     $booking_seat="";
     $booking_amount="";
      $booking_bustype="";
       $booking_date ="";
    $btn = "Save";
    $btnName = 'add';
    $pageTitle = 'Add';
  }
  
}


?>




  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class ="container">
         <h1 >
        booking
        <small>Edit Info</small>
        </h1>
        <ol class="breadcrumb">
        
           <li class="active">edit</li>
        </ol>
      </div>
    </section>
    <!-- Main content -->
   
    <div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>Booking details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
            
           
                 <?php if($this->session->flashdata('msg')) : ?>
                         <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                 <?php endif;?>
           <fieldset class="form-horizontal">
            <?php if(isset($booking_id)){ ?>
              <form role="form" action="<?php echo site_url('admin/booking/updatebooking/'.$booking_id);?>" method="post">
              <?php } else { ?>
                <form role="form" action="<?php echo site_url('admin/booking/addbooking/');?>" method="post">
              <?php } ?>


               
                    <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="fullname" class="control-label col-lg-3">Fullname</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="fullname" id="fullname" value="<?php echo $booking_data[0]->b_name;?>">
                    </div>
                </div>

                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="email" class="control-label col-lg-3">Email</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="email" class="form-control" name="email" id="email" value="<?php echo $booking_data[0]->b_name;?>">
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="password" class="control-label col-lg-3">Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="password" id="password" value="">
                    </div>
                </div>
                  <div class="form-group ">
                        <label for="route" class="control-label col-lg-3">Route Name *</label>
                        <div class="col-lg-9">
                            <select name="route" id="route" class="findTripByRouteDate form-control">
                                   <option value="" selected="selected">Select Option</option>
                                    <option value="Chittagong TO DHAKA">Chittagong TO DHAKA</option>
                                  <option value="Dhaka To sylhet">Dhaka To sylhet</option>
                                 <option value="Dhaka To sylhet">Dhaka To sylhet</option>
                               <option value="Dhaka To Jessore">Dhaka To Jessore</option>
                             <option value="Sylhet To Chittagong">Sylhet To Chittagong</option>
 
                          </select>
                     </div>
                </div>             
                 <div class="form-group ">
                        <label for="seat" class="control-label col-lg-3">Seat*</label>
                        <div class="col-lg-9">
                            <select name="seat" id="seat" class="findTripByRouteDate form-control">
                                   <option value="" selected="selected">Select Option</option>
                                    <option value="A1">A1</option>
                                  <option value="A2">A3</option>
                                 <option value="A3">A4</option>
                               <option value="A5">A5</option>
                             <option value="A7">A10</option>
                             <option value="A9">A7</option>
                             <option value="A10">A9</option>
 
                          </select>
                     </div>
                </div>         

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Amount</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="amount" id="amount" value="<?php echo $booking_data[0]->b_name;?>">
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">                               
                  <label for="phone" class="control-label col-lg-3">BUS type</label>   
                   <div class="col-lg-9">               
                        <select class="form-control" id="bus_type" name="bustype" required>                                                              
                                                    <option value="">Select</option>
                                                    <option value="AC"  >AC</option>
                                                    <option value="Non AC"  >Non AC</option>
                                                    <option value="AC Business Class"  >AC Business Class</option>                                                
                         </select> 
                   </div>
                   </div>                  

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="date" class="control-label col-lg-3">date</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="date" id="date" value="">
                    </div>
                </div>
            
              </form>
             </form>
                </fieldset>
            </div>
       
    </div>
   </div>    
             <hr class="hidden-md hidden-lg">
                  <div class="col-md-4 col-lg-2">
                      <div class="box-footer">
                         <button type="submit" name="update" class="btn btn-primary">Update</button>
                      </div>
                  </div>
                         
   </div>
 </div>
     