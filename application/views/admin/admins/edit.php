<?php 


if(isset($_POST['submit'])){
   $admins_name =  $admins_data[0]->a_name;
    $admins_email = $admins_data[0]->a_email;
    $admins_pass = $admins_data[0]->a_pass;
    $admins_phone = $admins_data[0]->phone;
      $admins_type = $admins_data[0]->a_type;
    $btn = "Update";
    $btnName = 'update';
    $pageTitle = 'Edit';
  }else{
    if(isset($_POST['submit'])){
    $admins_name = "";
    $admins_email = "";
    $admins_pass = "";
    $admins_phone = "";
      $admins_type="";
    $btn = "Save";
    $btnName = 'add';
    $pageTitle = 'Add';
  }
  
}

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class ="container">
         <h1 >
        admins
        <small>Edit Info</small>
        </h1>
        <ol class="breadcrumb">
        
           <li class="active">edit</li>
        </ol>
      </div>
    </section>
    <!-- Main content -->
   
    <div class="container">
    <div class="col-xs-12 detail_view ">
        <div class="panel panel-default">
             <div class="panel-heading">
                  <h3 class="panel-title"><strong>Admin details</strong></h3>
            </div>

        <div class="panel-body" id="customers_dv_container">
    
        <!-- form inputs -->
     

           
                 <?php if($this->session->flashdata('msg')) : ?>
                         <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                 <?php endif;?>
           <fieldset class="form-horizontal">
            <?php if(isset($admins_id)){ ?>
              <form role="form" action="<?php echo site_url('admin/admins/updateadmins/'.$admins_id);?>" method="post">
              <?php } else { ?>
                <form role="form" action="<?php echo site_url('admin/admins/addadmins/');?>" method="post">
              <?php } ?>


                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="fullname" class="control-label col-lg-3">Fullname</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="fullname" id="fullname"
                         value="<?php echo $admins_data[0]->a_name;?>" required >
                    </div>
                </div>

                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="email" class="control-label col-lg-3">Email</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="email" class="form-control" name="email" id="email" value="<?php echo $admins_data[0]->a_email;?>" required >
                    </div>
                  </div>
               
                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Phone</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="phone" id="phone" value="<?php echo $admins_data[0]->phone;?>">
                    </div>
                </div>
                
                  <div class="form-group">
                    <hr class="hidden-md hidden-lg">                               
                  <label for="phone" class="control-label col-lg-3">Admin type</label>   
                   <div class="col-lg-9">               
                        <select class="form-control" id="a_type" name="a_type" required>                                                              
                                                    <option value="">Select</option>
                                                    <option value="Sub-admin">Sub-admin</option>
                                                    <option value="Sub-agent">Sub-agent</option>
                                                                                                   
                         </select> 
                   </div>
                </div>        
               
               </form>
             </form>
                </fieldset>
            </div>
       
    </div>
   </div>    
             <hr class="hidden-md hidden-lg">
                  <div class="col-md-4 col-lg-2">
                      <div class="box-footer">
                         <button type="submit" name="update" class="btn btn-primary">Update</button>
                      </div>
                  </div>
                         
   </div>
 </div>
     