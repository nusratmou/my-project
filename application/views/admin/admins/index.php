
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
             
                <i class="fa fa-user" style="color: #000;font-size: 35px;">
                   <span style="font-size:20px;margin-right: 10px">users</span>
                  <a href="<?php echo site_url('admin/admins/add');?>" class="btn btn-success">Add <i class="fa fa-plus"></i></a></i>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if($this->session->flashdata('msg')) : ?>
              <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
            <?php endif;?>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sl No</th>
                  <th>FULL NAME</th>
                  <th>EMAIL</th>
                  <th>PHONE</th>
                  <th>ADDRESS</th>
                  <th>ADMIN TYPE</th>
                  <th>Action</th>
                </tr>
                </thead>
               <tbody>
  <?php $i=1;foreach ($admins_data as $admins) :  ?>
           
                  <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $admins->a_name;?></td>
                  <td><?php echo $admins->email;?></td>
                  <td><?php echo $admins->phone;?></td>
                   <td><?php echo $admins->a_type;?></td>
                   <td><?php echo $admins->address;?></td>
                
  
                  <td>
                    <a href="<?php echo site_url('admin/admins/edit/'.$admins->id);?>"><i class="fa fa-pencil"></i></a>
                    &nbsp;&nbsp;
                    <a href="<?php echo site_url('admin/admins/delete/'.$admins->id);?>" class='delete'><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
               <?php $i++; endforeach;?>
               
    </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    
