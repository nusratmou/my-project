
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      admins
        <small>Add Info</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("admin/dashboard");?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("admin/students");?>">Students</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">admins Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <!-- form inputs -->
        <form action="<?php echo site_url('admin/admins/addadmins');?>" method="post">
          <div class="panel-body" >
             <div class="col-md-8 col-lg-10">
            <fieldset class="form-horizontal">
                <?php if($this->session->flashdata('msg')) : ?>
                     <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                <?php endif;?>


                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="id" class="control-label col-lg-3">ID</label>
                    <div class="col-lg-9">
                        <div class="form-control-static" id="id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="fullname" class="control-label col-lg-3">Fullname</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="fullname" id="fullname" value="" required>
                    </div>
                </div>

                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="email" class="control-label col-lg-3">Email</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="email" class="form-control" name="email" id="email" value="" required>
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="password" class="control-label col-lg-3">Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="password" id="password" value="">
                    </div>
                </div>
                
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="con_password" class="control-label col-lg-3">Confirm Password</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="password" class="form-control" name="con_password" id="con_password" value="">
                    </div>
                </div>

                <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">Phone</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="phone" id="phone" value="">
                    </div>
                </div>
                 <div class="form-group">
                    <hr class="hidden-md hidden-lg">
                    <label for="phone" class="control-label col-lg-3">address</label>
                    <div class="col-lg-9">
                        <input maxlength="40" type="text" class="form-control" name="address"  value="">
                    </div>
                </div>
               
                <div class="form-group">
                    <hr class="hidden-md hidden-lg">                               
                  <label for="admin_type" class="control-label col-lg-3">Admin type</label>   
                   <div class="col-lg-9">               
                        <select class="form-control" id="admin_type" name="a_type" required>                                                              
                                                    <option value="" selected="selected">Select Option</option>
                                                    <option value="sub-admin">sub-admin</option>
                                                    <option value="sub-agent">sub-agent</option>
                                                   
                                                                                                   
                         </select> 
                   </div>
                   </div >           
             
           </fieldset>
         </div>
      </div>
     
         <div class="col-md-4 col-lg-2">
           <div class="btn-toolbar">
              <button type="submit" name="add" class="btn btn-success"> Save New</button>
            </div>
          </div>
                     
       </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>







