<!DOCTYPE html>
<html lang="en">
<head>
    <!--=== meta ===-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Page - Title</title>
    <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/owl.carousel.min.css">
    <!--=== css fixed ===-->
    <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/bootstrap.min.css">   
    <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/font-awesome.min.css"> <!-- Font Awesome V4.7.0 -->

    <!--=== custom css ===-->
    <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/login.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <a class="navbar-brand" href="#" >
                                    </a>
        <ul class="nav navbar-nav">
            <li>
                <a href="#"></a>
            </li>
            <li>
                <a href="#"></a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.html"><span class="glyphicon glyphicon-backward"></span> Return Home</a></li>
        </ul>
    </div>
</nav>


  <section class="main">
        <div class="panel-body">
            <?php if($this->session->flashdata('msg')) : ?>
                        <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                      <?php endif;?>

        <form method="post" action="<?php echo site_url('page/customer/');?>">
         <h1>Login</h1>
          <div class="form-group">
             <label for="exampleInputEmail1">Email address</label>
                         <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
                         <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
          <div class="checkbox">
            <label class="control-label" for="rememberMe">
              <input type="checkbox" name="rememberMe" id="rememberMe" value="1">
              Remember me           </label>
          </div>
          <div class="row">
            <!---div class="col-sm-offset-3 col-sm-6">
              <button name="do_login" type="submit" id="submit" value="login" class="btn btn-primary btn-lg btn-block">Login</button><span>
               <button name="do_login" type="submit" id="submit" value="login" class="btn btn-primary btn-lg btn-block">Login</button></span>
            </div-->
            
             <div class="form-group col-sm-6">
                            <input type="submit" class="btn btn-primary btn-lg " name="submit" value="Login">
                            <input type="reset" class="btn btn-danger btn-lg " name="submit" value="Order as a guest">
                        </div>
          </div>
                  
             <div class="reg">
              <a href="http://localhost/Project/page/creg" > Register Now ?</a>
                        </div>
      

        </form>​
        </div>

      </section>




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script  src="<?php echo base_url(); ?>resources/assets/js/bootstrap.min.js"></script>
    <script  src="<?php echo base_url(); ?>resources/assets/js/bootstrap.js"></script>
 
</body>
</html>




