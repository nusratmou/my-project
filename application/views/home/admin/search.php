 <section id="content" class="gray-area searchlist_box">
        <div class="container">
            <div id="main">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-container box searchlist">



                <?php if($this->session->flashdata('msg')) : ?>
                     <p class="bg-<?php echo $this->session->flashdata('type');?> text-center"><?php echo $this->session->flashdata('msg');?></p>
                <?php endif;?>
 
                           <div class="oneway text-center">
                            <h3><?php echo   $form_name ;?>  &nbsp; to  &nbsp;<?php echo $to_name ;?> &nbsp;<?php echo $doj = $_GET['doj'];?></h3>
                            </div>



                            <ul class="nav nav-tabs">
                                 <li class="active"><a href="">Departure</a></li>
                                 <li><a href="">Return</a></li>
                            </ul>
 
         
     
                            <div class="clearfix"></div>
                
            <div class="tab-content">
                                
                <div class="tab-pane fade in active" id="departure">
                    <div class="col-sm-3">
                        <div class="toggle-container filters-container">
                                            
                            <div class="panel style1">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#modify-search-panel" class="collapsed">Modify Search<i class="fa fa-plus" style=""></i></a>
                                    </h4><br>
                                <div id="modify-search-panel" class="panel-collapse collapse">
                                    <div class="panel-content">

                                                        
                                        <form name="bussearch" action="<?php echo site_url('page/search/');?>" method="get">
    
                                                 <div class="form-group">

                                                     <label for="from">Leaving From</label>
                                                        <select name="bfrom" id="bfrom" class="form-control" required>
                                                        <option value="" selected="selected">Select Option</option>
                                                        <?php foreach ($location_datas as $location_data) { ?>
                                                        <option value="<?php echo $location_data->title;?>"><?php echo $location_data->title;?></option>
                                                        <?php  } ?>
                                                        </select>
                                                 </div>
                    
                                                <div class="form-group">
                             
                                                    <label for="to">Going To</label>
                                                         <select name="bto" id="bto" class="form-control">
                                                         <option value="" selected="selected">Select Option</option>
                                                         <?php foreach ($location_datas as $location_data) { ?>
                                                         <option value="<?php echo $location_data->title;?>"><?php echo $location_data->title;?></option>
                                                         <?php  } ?>
                                                         </select>
                                                </div>

                        
                                                 <div class="form-group">
                                                    <label for="doj">Date of Journey</label>
                                                    <input type="text" class="form-control datepicker" name="doj"  id="doj" placeholder="Pick a date" value="">
                                                 </div>
                      
                  
                                                <div class="form-group">
                                                     <label for="dor">Date of Return</label>
                                                     <input type="text" class="form-control datepicker" name="dor"  id="dor"placeholder="Pick a date" value="">
                                                 </div>
                                                  </br>

                                                    <button type="button" class="btn btn-danger">search again</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                    <div class="col-sm-9">
                                        <div class="pn">
                                           <button type="button" class="btn btn-info"><i class="fa fa-angle-double-left"></i> Prev Day</button>
                                            <button type="button" class="btn btn-info departure_next">Next Day <i class="fa fa-angle-double-right"></i></button>
                                        </div>

                            <div class="table">
                                <tbody>
                                <table cellpadding="0" cellspacing="0" class="pj-table" style="width: 100%;">
                                    <thead>
                                        <tr>

                                            <th>
                                                 <div class="pj-table-sort-label">Bus name</div>
                                            </th>
                                             <th>
                                                <div class="pj-table-sort-label"> Dep. Time</div>
                                             </th>
                                             <th>
                                                 <div class="pj-table-sort-label"> Arr. Time</div>
           
                                             </th>
                                             <th>
                                                 <div class="pj-table-sort-label">Seats Available</div>
                                             </th>
                                              <th>
                                                 <div class="pj-table-sort-label">Fare
                                               
                                                 </div>
                                             </th>
                                              <th>
                                                 <div class="pj-table-sort-label">booking
                                               
                                                 </div>
                                             </th>
                                         </tr>
                                    </thead>
<tbody>
                                     <?php if($timing_data){ foreach ($timing_data as $timing) :  ?>
                                 <tr>
                                     <td>
                                        <?php echo $timing['title'];?>
                                   
                                    </td>
                
                                    <td> <?php echo $timing['time'];?></td>
                                    <td></td>
                                    <td>
                                        <?php 
                                        $booked_seat = $this->mod_home->total_booked_seat($timing['id'],'2019-02-05');
                                        $remain_seat = $timing['seat']-$booked_seat;
                                        echo $remain_seat."/".$timing['seat'];?>
                                            
                                        </td>
                                    <td><?php echo $timing['amount'];?></td>
                                    <td>
                                        
                                        </br>
                                        <?php if($remain_seat<0){
                                            echo "No Seat";
                                        }else{ ?>
                                         <a href="<?php echo site_url('page/search/seat/?from='.$from.'&bto='.$to.
                                         '&doj='.$doj.
                                         '&route='.$route_id.'&timing='.$timing['id']);?>" method="get" class="btn btn-success" style="padding: 1px 4px;">View seats</a>
                                     <?php } ?>
                                     </td>
                                </tr>

                              <?php endforeach; }?>
                                     

                                </tbody>
                                </table>  
                            </div>
                            </div>
                           </div>
                                </div>
                              
                                    </div>
                                  
                                    </div>
                                </div> 
                            </div>
                        </div>
                   
        </div>
</section>