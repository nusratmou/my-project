
<div class="col-md-12 text-center" style="margin: 100px 0;color:#D2D2D2;font-size:2em;padding: 0;border-bottom: 1px solid #dddddd;border-top: 1px solid #dddddd;">
    <h3 style="font-weight: bold;color:#079d49;">Bus Booking</h3>
</div>
                        <!-- ============= content starts here ================== -->
            <section id="content" class="container">

                <div class="clearfix">&nbsp;</div>
                <!-- 1st row starts -->
                <div class="row">
                    <!-- col-md-12 row starts -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <strong>NB:</strong> All (<i class="fa fa-star"></i>) marks are required field. 
                                    </div>
                                </div>
                                <form name="reservation" action="<?php echo site_url('page/reserved/show/');?>" method="get">
                                <div class="col-md-12">
                                    <legend>Journey Details</legend>
                                </div>
                                    <div class="col-lg-12">                                     
                                        <div class="col-md-4">                                                       
                                            <div class="form-group">
                                                
                                                 <div class="input-group date">
                                                    <div class="input-group-addon">
                                                       <i class="fa fa-calendar"></i>
                                                     </div>
                                                     <label for="doj">Date of Journey <i class="fa fa-star fa-1x"></i></label>
                                                     <input type="text" class="datepicker form-control" id="doj" name="doj"  placeholder="Pick a date" >
                                                </div>
                                            
                                             </div>
                                        </div>
                                        <div class="col-lg-4">                                            
                                            <div class="form-group">
                                                
                                                 <div class="input-group date">
                                                    <div class="input-group-addon">
                                                       <i class="fa fa-calendar"></i>
                                                    </div>
                                                <label for="dor">Date of Return <i class="fa fa-star fa-1x"></i></label>
                                                <input type="text" class="datepicker form-control" id="dor" name="dor"  placeholder="Pick a date">
                                               
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Bus Type <i class="fa fa-star"></i> </label> 
                                                <select name="title" id="" class="form-control">
                                                    <option value="" selected="selected">Select Option</option>
                                                       <?php foreach ($bus_type_datas as $bus_type_data) { ?>
                                                     <option value="<?php echo $bus_type_data->title;?>"><?php echo $bus_type_data->title;?></option>
                                 <?php  } ?>
 
                          </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Origin City ( with Boarding Point ) <i class="fa fa-star fa-1x"></i></label>
                                                <input name="origin" id="boardingPoint" class="form-control input-sm valid-empty" type="text" value="" required="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Destination City ( with Dropping Point ) <i class="fa fa-star fa-1x"></i> </label>
                                                <input name="destination" id="droppingPoint" class="form-control input-sm" type="text" value="" required="true">
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>No. of seats in bus <i class="fa fa-star fa-1x"></i> </label>
                                               <select name="seat" id="" class="form-control">
                                                      <option value="" selected="selected">Select Option</option>
                                                      <?php foreach ($bus_type_datas as $bus_type_data) { ?>
                                                      <option value="<?php echo $bus_type_data->seat;?>"><?php echo $bus_type_data->seat;?></option>
                                                      <?php  } ?>
                                               </select>                   
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>No. of buses required <i class="fa fa-star"></i></label>                                            
                                                <input name="numberOfBus" id="numberOfBus" class="form-control input-sm valid-empty" type="number" value="" required="true">
                                            </div>
                                        </div>
                                    </div> 


                                                                         
                                    <div class="col-lg-12"><br><br></div>
                                    <div class="col-md-12">
                                        <legend>Contact Details</legend>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Contact Person Name <i class="fa fa-star"></i></label>
                                                <input name="name" id="contactPersonName" class="form-control input-sm valid-empty" type="text" value="" required="true">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Mobile No. <i class="fa fa-star"></i></label>
                                                <input name="mobile" id="contactPersonMobile" class="form-control input-sm valid-empty" type="text" value="" maxlength="11" required="true">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" id="contactPersonEmail" class="form-control input-sm" type="email" value="">
                                            </div>
                                        </div>
                                    </div>                

                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Company Name</label>
                                                <input name="cname" id="companyName" class="form-control input-sm" type="text" value="">
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Address </label>
                                                <input name="address" id="contactPersonAddress" class="form-control input-sm" type="text" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="col-lg-12">
                                        <div class="col-lg-4 col-lg-offset-9">
                                            <div class="form-group" >
                                                <button class="btn btn-default " class="close" data-dismiss="alert" aria-label="close" type="submit"> Submit Reservation Request </button>
                                                 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                </div>
                <!--div id="tckt_print" class="col-md-12 clearfix">
                    <button type="submit" class="btn btn-default btn-sm">Print</button>
                </div-->
        </div>
        <!-- col-md-12 row ends -->
    </div>
    <br>

</section>



