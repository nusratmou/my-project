
   
<section class="main-heading">
       
     <div class="overlay">
           
        <div class="container">
               
             <div class="row">
               
            
                  <h2 class="title-text1 text-center">Online bus tickets booking!</h2>
                 
            

                     <!----- form start ========-->

                    <form name="bussearch" action="<?php echo site_url('page/search/');?>" method="get">
               
                                          
                        <div class="col-md-3">
                            <div class="form-group">

                              <label for="from">Leaving From</label>
                              <select name="bfrom" id="bfrom" class="form-control" required>
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($location_datas as $location_data) { ?>
                                      <option value="<?php echo $location_data->title;?>"><?php echo $location_data->title;?></option>
                                 <?php  } ?>
  
                          </select>
                            
                      </div>
                      <!--a href="serc/bus/?from=$from&to"></a-->
                    </div>
                        <div class="col-md-3">
                            <div class="form-group">
                             
                              <label for="to">Going To</label>
                               <select name="bto" id="bto" class="form-control">
                                     <option value="" selected="selected">Select Option</option>
                                  <?php foreach ($location_datas as $location_data) { ?>
                                      <option value="<?php echo $location_data->title;?>"><?php echo $location_data->title;?></option>
                                 <?php  } ?>
 
                          </select>
                
                       </div>

                        </div>

                       <div class="col-md-3">     
                            <div class="form-group">
                                <label for="doj">Date of Journey</label>
                                <input type="text" class="form-control datepicker" name="doj"  id="doj" placeholder="Pick a date" value="">
                            </div>
                       </div>
                      
                        
                      <div class='col-md-3'>
                           <div class="form-group">
                                <label for="dor">Date of Return</label>
                                <input type="text" class="form-control datepicker" name="dor"  id="dor"placeholder="Pick a date" value="">
                            </div>
                       </div>
                 
                   
                            <div class="header-info text-center">
               <button class="header-btn1" onclick="myFunction()"> Search Buses
       <span class="glyphicon glyphicon-search"></span> </button>
                     </div>     
                    </form> 

	              </div>
             </div>               
         </div>          
    </div>   

</section>


 
  <!-------------------------------------intro start---------------------------------->

<section id="intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="title-text2"><span>Welcome To Our service</span></h2>
        <p class="p-text">
        Green Line Paribahan is apparently a family owned transport company specializing in 
        transportation of passenger bus services since 1990.</br>
		From a humble beginning of 
        local services, our transport system encompasses all reachable areas of Bangladesh 
        and also beyond the border,</br> extending our reach to Kolkata in India.

          We worked hard and honest, we put our vision forward and explored ways and means 
          to continuously improve passenger comfort,</br> we were able to 
          introduce the 1st ever Air- Conditioned bus services in Bangladesh. We take 
          pride in mentioning that our fleet of buses includes the most luxurious models 
          of VOLVO and SCANIA Imported from Europe,</br> which provide the ultimate in passenger 
          comfort and safety.</br>

        
          The company at present operates more then 60 (Sixty) buses on schedule routes 
          employing over 200 trained staff and safely transporting over a million passengers a 
          year</br>Booking Bus Ticket with BookMyBusTicket</b> <br>
	With more than 1000 bus routes spread across the country and
	integration with over 1300 bus operators, MakeMyTrip's online bus 
	reservation system is simpler and smarter.<br> It provides you a wide 
	range of facilities, right from choosing your pickup point to your 
	preferred choice of seat (for instance, luxury buses with sleeper 
	berths).<br> You can also choose from the widest range of available 
	buses like Mercedes, Volvo, Volvo AC, AC luxury, Deluxe, Sleeper,
	Express and other private buses.<br>
	oo- you can use either debit/credit card facility or net-banking. Not just that, in case of any change in your travel plan, bus tickets can be cancelled online.
	So, next time you need not stand in long queues or search at different bus ticket counters. You can directly book bus tickets online with BookMyBusTicket.com and stay assured.
	</p>
      </div>
	  <div class="header-info2 text-center">
	             <button class="header-btn4">VIEW MORE</button>
			 </div>
     </div>
  </div>
</section>
<!-- ------------------------intro end---------------------- -->

<!------------------------details start---------------------->
<section id ="details">
     <div class="color-overlay">
        <div class="container">
            <div class="row">
             
			 <!-------------j start---------------------->
                <div class="j">
                    <div class="container p-none">
                        <ul class="p-none f-info-list">
                            <li>
                                <i class="fa fa-clock-o"></i>
                                <p class="p-text">You can book the tickets before one month
You can cancel the tickets one day before the journey with 100% refund</p>
                            </li>
                            <li>
                                <i class="fa fa-comments"></i>
                                <p class="p-text">You can comment/mail your queries to our contacts</p>
                            </li>
                            <li>
                                <i class="fa fa-user"></i>
                                <p class="p-text">You can create your personal account and can see your booking history</p>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <p class="p-text">Fax: +1 800 745 6024</p>
                            </li>
                        </ul>
		             </div>	 <!-------------j end---------------------->
		
                </div>
            </div>
        </div>
	</div>	
</section>
 <!------------------------details end-------------------------->
 
 
<div class="line"></div> 
 
 

