<!DOCTYPE html>
<html>
<head>
    <!--=== meta ===-->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Project - Home</title>
      <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/css/style.css?v=6.5"">
      <link rel="stylesheet" href="<?php echo base_url();?>resources/assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  
  

</head>
  <body>
     <div class="wrapper">
<!-- Navigation
    ==========================================-->
        <nav  class=" main navbar navbar-default ">
             <div class="container">        
<!-- Brand and toggle get grouped for better mobile display navbar-fixed-top -->
        
                 <div class="navbar-header">
          
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
          
                     </button>
            
                     <a class="navbar-brand" href="index.html"> Online bus system</a>
        
                 </div>

                 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          
                    <ul class="nav navbar-nav menu navbar-right">
       
                     <li><a class="active scroll" href="fhttp://localhost/Project/page/home">Home</a></li>
                     <li><a class="scroll" href="http://localhost/Project/page/about">About US</a></li>
                     <li><a class="scroll" href="http://localhost/Project/page/ticket">Ticket Cancellation</a></li>
                     <li><a class="scroll" href="http://localhost/Project/page/query">QUERY</a></li>
                     <li><a class="scroll" href="http://localhost/Project/page/reserved">RESERVATION</a></li>
                     <li><a class="scroll" href="#">CONTACT </a></li>
                     <li><a class="scroll" href="http://localhost/Project/admin/login/login">LOGIN </a></li>
                    
                    </ul>
        
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
   
<!-- [/NAV]

<! [/MAIN-HEADING]
===========================================================================================================================--> 
   
