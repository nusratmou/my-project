<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bustime extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_bustime');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."bustime";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['bustime_data']=$this->mod_bustime->get_bustimeinfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."bustime";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['bustime_id'] = $bustime_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."bustime";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['bustime_data']=$this->mod_bustime->get_bustimeinfo_by_id($bustime_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addbustime(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_bustime->insert_bustime($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg', 'User Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/bustime/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/bustime'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bustime'));
		}

	}
	public function updatebustime(){
		$bustime_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_bustime->update_bustime($postedData,$bustime_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/bustime'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/bustime/edit'.$bustime_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bustime'));
		}

	}

	public function delete(){
		$bustime_id = $this->uri->segment(4,0);
		
		$result = $this->mod_bustime->delete_bustime($bustime_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bustime'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/bustime'));
		}
		
	}

	



}
