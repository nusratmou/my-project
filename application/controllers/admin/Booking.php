<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_booking');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."booking";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['booking_data']=$this->mod_booking->get_bookinginfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."booking";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['booking_id'] = $booking_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."booking";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['booking_data']=$this->mod_booking->get_bookinginfo_by_id($booking_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addbooking(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_booking->insert_booking($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg','user Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/booking/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/booking'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/booking'));
		}

	}
	public function updatebooking(){
		$booking_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_booking->update_booking($postedData,$booking_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/booking'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/booking/edit'.$booking_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/booking'));
		}

	}

	public function delete(){
		$booking_id = $this->uri->segment(4,0);
		
		$result = $this->mod_booking->delete_booking($booking_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/booking'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/booking'));
		}
		
	}

	



}
