<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_user');
	    
	 }


	public function index()
	{
		if(!$this->session->userdata('logged_in') || $this->session->userdata('logged_in')==null){
			//error
			$this->session->set_flashdata('msg', 'You are Not Logged In.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/login'));
		}
		$data['dir']=$this->root_dir."user";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['user_data']=$this->mod_user->get_businfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."user";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['user_id'] = $user_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."user";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['user_data']=$this->mod_user->get_businfo_by_id($user_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function adduser(){
		
		$postedData =$this->input->post(NULL);

		

		// exit();



		// for datatable
		if(array_key_exists("add", $postedData)){

			$email = $postedData["email"];
			$password = $postedData['password'];
			$con_password = $postedData['con_password'];
			$fullname = $postedData['fullname'];
			$phone = $postedData['phone'];
			if($email=="" ||$password=="" ||$con_password=="" ||$fullname=="" ||$phone==""){
				$this->session->set_flashdata('msg', 'All Feild Required!');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/user/add'));
			}
			if($con_password!=$password){
				$this->session->set_flashdata('msg', 'Password and confirm password must be same.');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/user/add'));
			}


			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			 
			  $this->session->set_flashdata('msg', 'Invalid email format');
			  $this->session->set_flashdata('type', 'danger');
			  redirect(site_url('admin/user/add'));
			}

			$user_data = array(
				'b_name' => $fullname,
				'b_email'=>$email,
				'b_pass' => md5($password),
				'phone' => $phone
			);
			//exit;
			//update
			$result = $this->mod_user->insert_user($user_data);
			//exit;
			if($result==false){
				
				$this->session->set_flashdata('msg', 'user Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/user/add'));
			}else{
				$this->session->set_flashdata('msg', 'user Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/user'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/user'));
		}

	}

	public function updateuser(){
		$user_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);

		// for datatable
		if(array_key_exists("update", $postedData)){
			//update

			$email = $postedData["email"];
			$password = $postedData['password'];
			$con_password = $postedData['con_password'];
			$fullname = $postedData['fullname'];
			$phone = $postedData['phone'];
			if($email=="" ||$password=="" ||$con_password=="" ||$fullname=="" ||$phone==""){
				$this->session->set_flashdata('msg', 'All Feild Required!');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/user/edit'));
			}
			if($con_password!=$password){
				$this->session->set_flashdata('msg', 'Password and confirm password must be same.');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/user/edit'));
			}

			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			 
			  $this->session->set_flashdata('msg', 'Invalid email format');
			  $this->session->set_flashdata('type', 'danger');
			  redirect(site_url('admin/user/edit'));
			}

			$result = $this->mod_user->update_user($postedData,$user_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'user Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/user'));
			}else{
				$this->session->set_flashdata('msg', 'user Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/user/edit'.$user_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/user'));
		}

	}

	public function delete(){
		$user_id = $this->uri->segment(4,0);
		
		$result = $this->mod_user->delete_user($user_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'user Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/user'));
		}else{
			$this->session->set_flashdata('msg', 'user Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/user'));
		}
		
	}

	



}
