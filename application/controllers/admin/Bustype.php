<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bustype extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_bustype');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."bustype";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['bustype_data']=$this->mod_bustype->get_bus_typeinfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."bustype";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['route_datas']=$this->mod_bustype->get_routeinfo();
		$data['btime_datas']=$this->mod_bustype->get_btimeinfo();
	
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['bustype_id'] = $bustype_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."bustype";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['route_datas']=$this->mod_bustype->get_routeinfo();
		//$data['bustype_data']=$this->mod_bustype->get_bustypeinfo_by_id($bustype_id);
//
		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addbustype(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_bustype->insert_bustype($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg', 'User Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/bustype/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/bustype'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bustype'));
		}

	}
	public function updatebustype(){
		$bustype_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_bustype->update_bustype($postedData,$bustype_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/bustype'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/bustype/edit'.$bustype_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bustype'));
		}

	}

	public function delete(){
		$bustype_id = $this->uri->segment(4,0);
		
		$result = $this->mod_bustype->delete_bus_typeinfo($bustype_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bustype'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/bustype'));
		}
		
	}

	



}
