<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locatn extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_locatn');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."locatn";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['locatn_data']=$this->mod_locatn->get_locatninfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."locatn";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['locatn_id'] = $locatn_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."locatn";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['locatn_data']=$this->mod_locatn->get_locatninfo_by_id($locatn_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addlocatn(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_locatn->insert_locatn($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg','user Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/locatn/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/locatn'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/locatn'));
		}

	}
	public function updatelocatn(){
		$locatn_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_locatn->update_locatn($postedData,$locatn_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/locatn'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/locatn/edit'.$locatn_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/locatn'));
		}

	}

	public function delete(){
		$locatn_id = $this->uri->segment(4,0);
		
		$result = $this->mod_locatn->delete_locatn($locatn_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/locatn'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/locatn'));
		}
		
	}

	



}
