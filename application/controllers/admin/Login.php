
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Login Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_log');
	    
	}


	public function login()
	{
		
		if(!$this->session->userdata('logged_in') || $this->session->userdata('logged_in')==null){
			$data['resource_dir'] = base_url()."resources/admin/";
			
			$this->load->view($this->root_dir.'login',$data);
		}else{
			//error
			$this->session->set_flashdata('msg', 'You are Already Logged In.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/dashboard'));
		}
		
	}

	function do_login(){
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("do_login", $postedData)){
			$email = $postedData['email'];
			$password = $postedData['password'];
			$do_login = $this->mod_log->do_login($email,$password);
			
			if(!$do_login){
				//error
				$this->session->set_flashdata('msg', 'Wrong Email/Password.');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/login'));
			}else{


				$newdata = array(
				        'name'  => '',
				        'login_id'  => '',
				        'email'     => $do_login[0]->email,
				        'logged_in' => TRUE,
				        'logged_in_type' => $do_login[0]->type
				);
				$this->session->set_userdata($newdata);
				//success
				$this->session->set_flashdata('msg', 'Login Successfully.');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/dashboard'));
			}
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('login/login'));
		}
	}

	function do_logout(){
		$newdata = array('name','email','logged_in','logged_in_type');
		$this->session->unset_userdata($newdata);
		//success
		$this->session->set_flashdata('msg', 'Logout Successfully.');
		$this->session->set_flashdata('type', 'success');
		redirect(site_url('admin/login/login'));
	}
	



}
