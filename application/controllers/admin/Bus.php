<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bus extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_bus');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."bus";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['bus_data']=$this->mod_bus->get_businfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."bus";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['bus_id'] = $bus_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."bus";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['bus_data']=$this->mod_bus->get_businfo_by_id($bus_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addbus(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_bus->insert_bus($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg', 'User Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/bus/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/bus'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bus'));
		}

	}
	public function updatebus(){
		$bus_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_bus->update_bus($postedData,$bus_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/bus'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/bus/edit'.$bus_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bus'));
		}

	}

	public function delete(){
		$bus_id = $this->uri->segment(4,0);
		
		$result = $this->mod_bus->delete_bus($bus_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/bus'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/bus'));
		}
		
	}

	



}
