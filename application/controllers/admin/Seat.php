<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seat extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_seat');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."seat";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['seat_data']=$this->mod_seat->get_seatinfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."seat";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['seat_id'] = $seat_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."seat";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['seat_data']=$this->mod_seat->get_seatinfo_by_id($seat_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addseat(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_seat->insert_seat($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg','user Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/seat/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/seat'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/seat'));
		}

	}
	public function updateseat(){
		$seat_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);

		// for datatable
		if(array_key_exists("update", $postedData)){
			//update


			$result = $this->mod_seat->update_seat($postedData,$seat_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'seat Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/seat'));
			}else{
				$this->session->set_flashdata('msg', 'seat Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/seat/edit'.$seat_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/seat'));
		}

	}

	public function delete(){
		$seat_id = $this->uri->segment(4,0);
		
		$result = $this->mod_seat->delete_seat($seat_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'seat Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/seat'));
		}else{
			$this->session->set_flashdata('msg', 'seat Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/seat'));
		}
		
	}

	



}
