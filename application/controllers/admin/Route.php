<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Route extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_route');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."route";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['route_data']=$this->mod_route->get_routeinfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."route";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['location_datas']=$this->mod_route->get_locationinfo();
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['route_id'] = $route_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."route";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['route_data']=$this->mod_route->get_routeinfo_by_id($route_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addroute(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_route->insert_route($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg','user Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/route/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/route'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/route'));
		}

	}
	public function updateroute(){
		$route_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_route->update_route($postedData,$route_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/route'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/route/edit'.$route_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/route'));
		}

	}

	public function delete(){
		$route_id = $this->uri->segment(4,0);
		
		$result = $this->mod_route->delete_route($route_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/route'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/route'));
		}
		
	}

	



}
