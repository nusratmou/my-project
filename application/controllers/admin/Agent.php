<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class agent extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_agent');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."agent";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['agent_data']=$this->mod_agent->get_agentinfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."agent";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['agent_id'] = $agent_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."agent";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['agent_data']=$this->mod_agent->get_agentinfo_by_id($agent_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addagent(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$data = array(
				 'pass' => md5($pass),
                 'type' => $agent
           
			);
			$agent_id = $this->mod_agent->insert_agent($postedData);

			if($agent_id){
		                $add_info_user_agent = $this->mod_reg->add_info_user_agent($user_id,$postedData);
					 }else {

					 }
           
			};

			$result = $this->mod_agent->insert_agent($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg','user Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/agent/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/agent'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/agent'));
		}

	}
	public function updateagent(){
		$agent_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_agent->update_agent($postedData,$agent_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/agent'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/agent/edit'.$agent_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/agent'));
		}

	}

	public function delete(){
		$agent_id = $this->uri->segment(4,0);
		
		$result = $this->mod_agent->delete_agent($agent_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/agent'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/agent'));
		}
		
	}

	



}
