<<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admins extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_admins');
	   
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."admins";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['admins_data']=$this->mod_admins->get_adminsinfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."admins";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['admins_id'] = $admins_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."admins";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['admins_data']=$this->mod_admins->get_adminsinfo_by_id($admins_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addadmins(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
                $a_name = $postedData['fullname'];
             // $a_name=>$postedData['fullname'];
                $email=$postedData['email'];
                $pass =md5($postedData['password']);
                $phone = $postedData['phone'];
              //  $a_type =$postedData['a_type'];
                  $address =$postedData['address'];
               $type = 'sub-admin';

                $make_user = $this->mod_admins->make_user($email,$pass,$type);
			 	if(!$make_user){
			 		//echo "Not added";
			 		 $this->session->set_flashdata('msg', 'Wrong Email/Password.');
			 		 $this->session->set_flashdata('type', 'danger');
			 		 redirect(site_url('admin/admins/add'));
		 	        }else{   

                         $login_id = $make_user;
           

					 $admin_data = array(
			                           'a_name' => $a_name,
				                      'email' => $email,
				                       'phone' => $phone,
				                       'address' => $address,
			                           'login_id' => $login_id 
			                          );
					// print_r($admin_data);
			//exit;
			//update
			$result = $this->mod_admins->insert_admins($admin_data,$postedData);  
		}
 //print_r($result);
			//exit;
			//exit;
			if($result==false){
				
				$this->session->set_flashdata('msg', 'User Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/admins/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/admins'));
			}
			return true;
		
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/admins'));
		}

	}

	public function updateadmins(){
		$admins_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_admins->update_admins($postedData,$admins_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/admins'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/admins/edit'.$admins_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/admins'));
		}

	}

	public function delete(){
		$admins_id = $this->uri->segment(4,0);
		
		$result = $this->mod_admins->delete_admins($admins_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/admins'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/admins'));
		}
		
	}

	



}
