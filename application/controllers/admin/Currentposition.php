<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currentposition extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_cposition');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."currentposition";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['currentposition_data']=$this->mod_cposition->get_cpositioninfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."currentposition";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['location_datas']=$this->mod_cposition->get_locationinfo();
		$data['bus_info_datas']=$this->mod_cposition->get_businfo();
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['currentposition_id'] = $currentposition_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."currentposition";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['currentposition_data']=$this->mod_cposition->get_cpositioninfo_by_id($currentposition_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addcurrentposition(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_cposition->insert_cposition($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg', 'User Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/currentposition/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/currentposition'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/currentposition'));
		}

	}
	public function updatecposition(){
		$currentposition_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_cposition->update_cposition($postedData,$currentposition_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/currentposition'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/currentposition/edit'.$currentposition_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/currentposition'));
		}

	}

	public function delete(){
		$currentposition_id = $this->uri->segment(4,0);
		
		$result = $this->mod_cposition->delete_cpositioninfo($currentposition_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/currentposition'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/currentposition'));
		}
		
	}

	



}
