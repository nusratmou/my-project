<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    
	 }


	public function index()
	{
		if(!$this->session->userdata('logged_in') || $this->session->userdata('logged_in')==null){
			//error
			$this->session->set_flashdata('msg', 'You are Not Logged In.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/login'));
		}
		$data['dir']=$this->root_dir."dashboard";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/admin/";
	
		$this->load->view($this->root_dir.'dashboard/main',$data);

	}
	



}
