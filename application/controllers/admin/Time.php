<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_time');
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."time";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['time_data']=$this->mod_time->get_timeinfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		$data['dir']=$this->root_dir."time";
		$data['page']="add";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['route_datas']=$this->mod_time->get_routeinfo();
		$data['bus_type_datas']=$this->mod_time->get_bus_typeinfo();
		$data['btime_datas']=$this->mod_time->get_btimeinfo();
		$this->load->view($this->root_dir.'user/main',$data);
	}

	public function edit(){
		$data['timing_id'] = $timing_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."time";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['time_data']=$this->mod_time->get_timeinfo_by_id($timing_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addtime(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_time->insert_time($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg', 'User Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/time/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/time'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/time'));
		}

	}
	public function updatetime(){
		$timing_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_time->update_time($postedData,$timing_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/time'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/time/edit'.$timing_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/time'));
		}

	}

	public function delete(){
		$timing_id = $this->uri->segment(4,0);
		
		$result = $this->mod_time->delete_timeinfo($timing_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/time'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/time'));
		}
		
	}

	



}
