<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {


	/**
	 * Login Page for my customer
	 *
	 */
	private $root_dir = "home/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('admin/mod_log');
	     $this->load->model('admin/mod_home');
	    
	}


	public function index()
	{
		
		if(!$this->session->userdata('logged_in') || $this->session->userdata('logged_in')==null){
			$data['resource_dir'] = base_url()."resources/home/";
			
			$this->load->view($this->root_dir.'index',$data);
		}else{

			$cusData['route_id'] = $this->input->post('route_id');
			$cusData['doj'] = $this->input->post('doj');
			$cusData['timing_id'] = $this->input->post('timing_id');
			$cusData['bus_fare'] = $this->input->post('bus_fare');
			$cusData['seatNo'] = $this->input->post('seatNo');
			$cusData['indexNo'] = $this->input->post('indexNo');
			$cusData['busType'] = $this->input->post('busType');
			$book_table = $this->mod_home->booking_code($cusData);

			$this->session->set_flashdata('msg', 'You are Already Logged In.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('page/booked'));
		}
		
	}

	function login(){
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("login", $postedData)){
			$email = $postedData['email'];
			$password = $postedData['password'];
			$login = $this->mod_log->login($email,$password);
			
			if(!$login){
				//error
				$this->session->set_flashdata('msg', 'Wrong Email/Password.');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('page/customer'));
			}else{


				$newdata = array(
				        'name'  => '',
				        'login_id'  => $login[0]->id,
				        'email'     => $login[0]->email,
				        'logged_in' => TRUE,
				        'logged_in_type' => $login[0]->type
				);
				$this->session->set_userdata($newdata);
				//success
				$this->session->set_flashdata('msg', 'Login Successfully.');
				$this->session->set_flashdata('type', 'success');
				//controller
				redirect(site_url('page/booked'));
			}
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('page/customer'));
		}
	}

	function logout(){
		$newdata = array('name','email','logged_in','logged_in_type');
		$this->session->unset_userdata($newdata);
		//success
		$this->session->set_flashdata('msg', 'Logout Successfully.');
		$this->session->set_flashdata('type', 'success');
		redirect(site_url('page/customer'));
	}
	



}
