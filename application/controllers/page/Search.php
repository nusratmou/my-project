<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "home/";
	public function __construct()
	{
	       parent::__construct();
	    $this->load->model('admin/mod_home');
	    
	 }



	public function index()
	{
		// echo "<pre>";
		// print_r($_GET);
		// exit;
		 $data['from'] = $from = $_GET['from'];
		 $data['to'] = $to = $_GET['bto'];
		 $data['form_name']= $form_name = $this->mod_home->get_location_name($from);
		 $data['to_name']= $to_name = $this->mod_home->get_location_name($to);
		 if($form_name==false){
		 	//echo "Sorry From location is invalid";
		 	 $this->session->set_flashdata('msg', 'Sorry From location is invalid');
	 		 $this->session->set_flashdata('type', 'success');
	 		 redirect(site_url('page/search'));
		 }
		 if($to_name==false){
		 	//echo "Sorry From location is invalid";
		 	 $this->session->set_flashdata('msg', 'Sorry From location is invalid');
	 		 $this->session->set_flashdata('type', 'success');
	 		 redirect(site_url('page/search'));
		 }
		 if($from==$to){
		 	//error
			   $this->session->set_flashdata('msg', 'User Data In is');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('page/search'));
		 }

 
	//	$form= $this->mod_home->get_location_name('from');
		//$to = $this->mod_home->get_location_name('to');
		//	1 $this->mod_home->get_location_name($from,$to);
		 //
		 

		  $data['route_id']=$route_id =$this->mod_home->check_route($from,$to);

		// $data['bus_type_id']=$bus_type_id =$this->mod_home->get_timinginfo($bus_type_id);
	
		 if($route_id==false){

			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('page/search')); 
		 }		
		 // echo "<pre>";
		 //print_r($data);
		 //exit;
																				
		$data['dir']=$this->root_dir."admin";
		$data['page']="search";
		$data['resource_dir'] = base_url()."resources/assets/";
		//$data['bustype_data']=$this->mod_home->get_bustypeinfo();
		$data['timing_data']=$this->mod_home->get_timinginfo($route_id);
			
	    //$data['location_datas'] = $this->mod_home->get_locationinfo();
		//$data['route_datas'] = $this->mod_home->get_routeinfo();
        	
		$this->load->view($this->root_dir.'main',$data);
	}


	function add_seat_book_temp(){
		
		$doj = $_POST['doj'];
		$seat_no = $_POST['seat_no'];
		$indexNo = $_POST['indexNo'];
		$busType = $_POST['busType'];
		$busFare = $_POST['busFare'];
		//$array_id = $newdata['seats'][$doj];

		//$this->session->unset_userdata('seats');

		$seats = $this->session->userdata('seats');
		if(!$seats){
			$seats[$doj] = array();
		}
		$data = $seats[$doj];

		$array_data =  array(
	        			'seat_no' => $seat_no,
	        			'indexNo' => $indexNo,
	        			'busType' => $busType,
	        			'busFare' => $busFare,
	        			'doj' => $doj,
	        		
		        	);
		$data[$seat_no]=$array_data;
		$newdata['seats'][$doj] = $data; 
	   $this->session->set_userdata($newdata);

		// echo "<pre>";
		// print_r($seats);
	   echo "done";
		exit;
	}

	public function seat()
	{
		$data['from'] = $from = $_GET['from'];
		$data['to'] = $to = $_GET['bto'];
		$data['timing_id'] = $timing_id = $_GET['timing'];
		$data['doj'] = $doj = $_GET['doj'];
		$data['form_name']= $form_name = $this->mod_home->get_location_name($from);
		$data['to_name']= $to_name = $this->mod_home->get_location_name($to);

		$data['route_id']=$route_id =$this->mod_home->check_route($from,$to);
	//$data['timing_id']=$timing_id =$this->mod_home->get_timinginfo($timing_id);
		// $data['bus_type_id']=$bus_type_id =$this->mod_home->get_timinginfo($bus_type_id);
		
		 if($route_id==false){

			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/home/')); 
		 }	
		$data['timing_data']= $timing_data = $this->mod_home->get_timinginfo_by_id($timing_id);
		//$data['book_data']= $book_data = $this->mod_home->get_bookinginfo_id();
		$data['seat'] = $timing_data[0]['seat'];
		$data['bus_title'] = $timing_data[0]['title'];
		$data['bus_fare'] = $timing_data[0]['amount'];
		//$data[''] = $timing_data[0]['amount'];
// echo $timing_id;
// echo $doj;

		$data['booking_seat'] =$this->mod_home->booked_seat($timing_id,$doj);


		 																																							
		$data['dir']=$this->root_dir."admin";
		$data['page']="seat";
		$data['resource_dir'] = base_url()."resources/assets/";
			//$data['timing_data']=$this->mod_home->get_timinginfo($timing_id);
			//$data['location_datas'] = $this->mod_home->get_locationinfo();
		//$data['route_datas'] = $this->mod_home->get_routeinfo();
		$this->load->view($this->root_dir.'main',$data);
	}
	
   
}
   

