<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reserved extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "home/";
	public function __construct()
	{
	       parent::__construct();
	    $this->load->model('admin/mod_reserve');
	    
	 }



	public function index()
	{
		$data['dir']=$this->root_dir."admin";
		$data['page']="reserved";
		$data['resource_dir'] = base_url()."resources/assets/";
	   $data['bus_type_datas']=$this->mod_reserve->get_bus_typeinfo();
	    //$data['booking_data'] = $this->mod_reserved->get_bookinginfo();
		$this->load->view($this->root_dir.'main',$data);
	}

 
	public function addbus(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_reserved->insert_bus($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg','user Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/route/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/route'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/route'));
		}

	}



	public function show()
	{
																																													
		$data['dir']=$this->root_dir."admin";
		$data['page']="show";
		$data['resource_dir'] = base_url()."resources/assets/";
		 $data['bus_type_datas']=$this->mod_reserve->get_bus_typeinfo();
		//$data['route_datas'] = $this->mod_home->get_routeinfo();
		$this->load->view($this->root_dir.'main',$data);
	}
	
   
}

   
