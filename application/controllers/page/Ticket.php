<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "home/";
	public function __construct()
	{
	    parent::__construct();
	    
	 }


	public function index()
	{
		
		$data['dir']=$this->root_dir."admin";
		$data['page']="ticket";
		$data['resource_dir'] = base_url()."resources/assets/";
	
		$this->load->view($this->root_dir.'main',$data);
	}
}