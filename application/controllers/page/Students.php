<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "admin/";
	public function __construct()
	{
	    parent::__construct();
	    $this->load->model($this->root_dir.'mod_students');
	    
	 }


	public function index()
	{
		if(!$this->session->userdata('logged_in') || $this->session->userdata('logged_in')==null){
			//error
			$this->session->set_flashdata('msg', 'You are Not Logged In.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/login'));
		}
		
		$data['dir']=$this->root_dir."students";
		$data['page']="index";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['st_data']=$this->mod_students->get_stinfo();

	
		$this->load->view($this->root_dir.'main',$data);
	}

	public function add(){
		
		if(!$this->session->userdata('logged_in') || $this->session->userdata('logged_in')==null){
			//error
			$this->session->set_flashdata('msg', 'You are Not Logged In.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/login'));
		}
		$data['dir']=$this->root_dir."students";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$this->load->view($this->root_dir.'main',$data);
	}

	public function edit(){
		if(!$this->session->userdata('logged_in') || $this->session->userdata('logged_in')==null){
			//error
			$this->session->set_flashdata('msg', 'You are Not Logged In.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/login'));
		}
		$data['student_id'] = $student_id = $this->uri->segment(4,0);
		$data['dir']=$this->root_dir."students";
		$data['page']="edit";
		$data['resource_dir'] = base_url()."resources/admin/";
		$data['st_data']=$this->mod_students->get_stinfo_by_id($student_id);

		
		$this->load->view($this->root_dir.'main',$data);
	}

	public function addStudent(){
		
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("add", $postedData)){
			//update
			$result = $this->mod_students->insert_student($postedData);
			if($result==false){
				
				$this->session->set_flashdata('msg', 'User Data Inserted Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/students/add'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Inserted Successfully.New ID is'.$result);
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/students'));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/students'));
		}

	}
	public function updateStudent(){
		$student_id = $this->uri->segment(4,0);
		$postedData =$this->input->post(NULL);



		// for datatable
		if(array_key_exists("update", $postedData)){
			//update
			$result = $this->mod_students->update_student($postedData,$student_id);
			if($result==true){
				$this->session->set_flashdata('msg', 'User Data Updated Successfully');
				$this->session->set_flashdata('type', 'success');
				redirect(site_url('admin/students'));
			}else{
				$this->session->set_flashdata('msg', 'User Data Updated Failed');
				$this->session->set_flashdata('type', 'danger');
				redirect(site_url('admin/students/edit'.$student_id));
			}
			return true;
		}else{
			//error
			$this->session->set_flashdata('msg', 'You Are Not Authorised.');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/students'));
		}

	}

	public function delete(){
		$student_id = $this->uri->segment(4,0);
		
		$result = $this->mod_students->delete_student($student_id);

		if($result==false){
			
			$this->session->set_flashdata('msg', 'User Data Deleted Failed');
			$this->session->set_flashdata('type', 'danger');
			redirect(site_url('admin/students'));
		}else{
			$this->session->set_flashdata('msg', 'User Data Deleted Successfully.');
			$this->session->set_flashdata('type', 'success');
			redirect(site_url('admin/students'));
		}
		
	}

	



}
