<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Dashboard Page for my Admin
	 *
	 */
	private $root_dir = "home/";
	public function __construct()
	{
	       parent::__construct();
	    $this->load->model('admin/mod_home');
	    
	 }


	public function index()
	{
																																																		
		$data['dir']=$this->root_dir."admin";
		$data['page']="home";
		$data['resource_dir'] = base_url()."resources/assets/";
		$data['location_datas'] = $this->mod_home->get_locationinfo();
		$data['route_datas'] = $this->mod_home->get_routeinfo();
		$this->load->view($this->root_dir.'main',$data);
	}
	
   
}