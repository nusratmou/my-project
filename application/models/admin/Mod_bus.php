<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for bus controller
*@param 
*@return 
*@author 
*@version 1.0 (10-27-2018)
*/
class Mod_bus extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_businfo(){
    	$query = $this->db->get('bus_info');
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for getting info of individual bus by     *their id.
    *@param $b_id bus Id
    *@return $result bus info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_businfo_by_id($id){
    	$where =array(
    		"id" => $id
    	);
    	$query = $this->db->get_where('bus_info',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting bus info
    *@param $PostData array bus data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_bus($postData){
    	
     $datas = array(
            'bus_name' => $postData['bus_name'],
           'bus_rnumbr' => $postData['b_rnumber'],
            
          

        );
    	$query = $this->db->insert('bus_info',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng bus info
    *@param $PostData array bus data
    *@param $bus ID Int bus unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_bus($postData,$id){
    	$where =array(
    		"id" => $id
    	);
    	 $datas = array(
            'bus_name' => $postData['bus_name'],
           'bus_rnumbr' => $postData['b_rnumber'],
           
        );
    	$query = $this->db->update('bus_info',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting bus info
    *@param $PostData array bus data
    *@param $bus ID Int bus unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_bus($b_id){
    	$where =array(
    		"id" => $b_id
    	);
    	
    	$query = $this->db->delete('bus_info',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    