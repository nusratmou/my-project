<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for home controller
*@param 
*@return 
*@author Tj Thouhid
*@version 1.0 (10-27-2018)
*/
class Mod_user extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_businfo(){
    	$query = $this->db->get('bus_user');
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for getting info of individual student by     *their id.
    *@param $s_id Student Id
    *@return $result Student info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_businfo_by_id($id){
    	$where =array(
    		"id" => $id
    	);
    	$query = $this->db->get_where('bus_user',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting Student info
    *@param $PostData array student data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_user($postData){
     //    echo "<pre>";
     //    print_r($postData);
    	 $postData['inserted'] = date("Y-m-d h:i:s");

     //    echo "<pre>";
     //    print_r($postData);
    	// exit;


    	$query = $this->db->insert('bus_user',$postData );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng Student info
    *@param $PostData array student data
    *@param $student ID Int student unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_user($postData,$user_id){
    	$where =array(
    		"id" => $user_id
    	);
        
    	   $datas = array(
            'b_name' => $postData['fullname'],
            'b_email' => $postData['email'],
             'b_pass' => md5($postData['password']),
           'phone' => $postData['phone'],
         
               
            );
    	$query = $this->db->update('bus_user',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting Student info
    *@param $PostData array student data
    *@param $student ID Int student unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_user($user_id){
    	$where =array(
    		"id" => $user_id
    	);
    	
    	$query = $this->db->delete('bus_user',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    