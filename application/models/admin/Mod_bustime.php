<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for bustime controller
*@param 
*@return 
*@author 
*@version 1.0 (10-27-2018)
*/
class Mod_bustime extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_bustimeinfo(){
    	$query = $this->db->get('bustime');
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for getting info of individual bustime by     *their id.
    *@param $b_id bustime Id
    *@return $result bustime info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_bustimeinfo_by_id($id){
    	$where =array(
    		"id" => $id
    	);
    	$query = $this->db->get_where('bustime',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting bustime info
    *@param $PostData array bustime data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_bustime($postData){
    	
     $datas = array(
            

           'b_type' => $postData['b_type'],
            'time' => $postData['time'],
           
          

        );
    	$query = $this->db->insert('bustime',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng bustime info
    *@param $PostData array bustime data
    *@param $bustime ID Int bustime unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_bustime($postData,$id){
    	$where =array(
    		"id" => $id
    	);
    	 $datas = array(
              

           'b_type' => $postData['b_type'],
            'time' => $postData['time'],
           

        );
    	$query = $this->db->update('bustime',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting bustime info
    *@param $PostData array bustime data
    *@param $bustime ID Int bustime unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_bustime($id){
    	$where =array(
    		"id" => $id
    	);
    	
    	$query = $this->db->delete('bustime',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    