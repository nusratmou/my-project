<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Mod_search extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

 
   function get_timeinfo(){
        $sql="SELECT timing.id as timing_id,route.id, btime.time as bus_tym,bus_type.title as bus_typeL, CONCAT(l1.title,'-',l2.title) as route_name FROM `timing` left join route on route.id = timing.route_id left join btime on btime.id = timing.bus_time left join bus_type on bus_type.id=timing.bus_type_id left join location l1 on l1.id=route.bus_from left join location l2 on l2.id = route.bus_to";
        $query = $this->db->query($sql);
        return $query->result();
        
        

    
    }


    function routeinfo(){
        $sql = "SELECT route.id,l1.title as bus_from_title,l2.title as bus_to_title FROM `route` left JOIN location l1 ON l1.id = route.bus_from left join location l2 ON l2.id = route.bus_to";
    	$query = $this->db->get(query($sql));
    	return $query->result();
    	
    }

    function get_locationinfo(){
        $query = $this->db->get('location');
        return $query->result();
    }
}
