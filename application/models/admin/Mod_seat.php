<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for home controller
*@param 
*@return 
*@author Tj Thouhid
*@version 1.0 (10-27-2018)
*/
class Mod_seat extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_seatinfo(){
        $query = $this->db->get('bus_seat');
        return $query->result();
        
    }


    /**
    *
    *Description : This method for getting info of individual student by     *their id.
    *@param $s_id Student Id
    *@return $result Student info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_seatinfo_by_id($id){
        $where =array(
            "id" => $id
        );
        $query = $this->db->get_where('bus_seat',$where);
        return $query->result();
        
    }


    /**
    *
    *Description : This method for Inserting Student info
    *@param $PostData array student data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_seat($postData){
        
    $datas = array(
            'b_name' => $postData['fullname'],
            'b_stype' => $postData['seattype']
       
          

        );
        $query = $this->db->insert('bus_seat',$datas );
        if($query){
            return $insert_id = $this->db->insert_id();
            
        }else{
            return false;
        }
        
    }
    /**
    *
    *Description : This method for Updatng seat info
    *@param $PostData array seat data
    *@param $seat ID Int seat unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_seat($postData,$seat_id){
        $where =array(
            "id" => $seat_id
        );
       $datas = array(
            'b_name' => $postData['fullname'],
            'b_stype' => $postData['seattype']
       
          

        );
        $query = $this->db->update('bus_seat',$datas ,$where);
        if($query){
            return true;
        }else{
            return false;
        }
        
    } 



    /**
    *
    *Description : This method for Deleting seat info
    *@param $PostData array seat data
    *@param $seat ID Int seat unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_seat($seat_id){
        $where =array(
            "id" => $seat_id
        );
        
        $query = $this->db->delete('bus_seat',$where); 
        if($query){
            return true;
        }else{
            return false;
        }
        
    }


}

    