<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for admins controller
*@param 
*@return 
*@author 
*@version 1.0 (10-27-2018)
*/
class Mod_admins extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_adminsinfo(){
    	$query = $this->db->get('admins');
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for getting info of individual admins by     *their id.
    *@param $s_id admins Id
    *@return $result admins info
    *@author
    *@version 1.0 (10-27-2018)
    */

    function get_adminsinfo_by_id($admins_id){
    	$where =array(
    		"id" => $admins_id
    	);
    	$query = $this->db->get_where('admins',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting admins info
    *@param $PostData array admins data
    *@return $result True/False
    *@author
    *@version 1.0 (11-04-2018)
    */

    function insert_admins($postData){
    	
    	$datas = array(
                 'a_name' => $postData['fullname'],
                'a_email'=>$postData['email'],
                'a_pass' =>md5($postData['password']),
                'phone' => $postData['phone'],
                'a_type' =>$postdata['a_type']
           
    	);
    	$query = $this->db->insert('admins',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng admins info
    *@param $PostData array admins data
    *@param $admins ID Int admins unique id
    *@return $result True/False
    *@author
    *@version 1.0 (11-04-2018)
    */

    function update_admins($postData,$admins_id){
    	$where =array(
    		"id" => $admins_id
    	);
    	$datas = array(
               'a_name' => $postData['fullname'],
                'a_email'=>$postData['email'],
                'a_pass' =>md5($postData['password']),
                'phone' => $postData['phone'],
                'a_type' =>$postdata['a_type']
           
        );
    	$query = $this->db->update('admins',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting admins info
    *@param $PostData array admins data
    *@param $admins ID Int admins unique id
    *@return $result True/False
    *@author
    *@version 1.0 (11-04-2018)
    */

    function delete_admins($admins_id){
    	$where =array(
    		"id" => $admins_id
    	);
    	
    	$query = $this->db->delete('admins',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    