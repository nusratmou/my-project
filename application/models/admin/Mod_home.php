<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Mod_home extends CI_Model {
 
 

 
    public function __construct()
    {   
        parent::__construct();
        

    }

   function get_location_name($location_id){
       $this->db->select('title');
       $this->db->from('location');
       $this->db->where (array('id'=> $location_id));
       $query = $this->db->get();
      if($query->num_rows()>0){
        $result = $query->result_array();
        //print_r($result);
        return $result[0]['title'];
      }else{
        return false;
      }
       
    
  }


  function total_booked_seat($timming_id,$date){
    $sql = "SELECT b.id FROM `booking` as b WHERE b.timing_id = '$timming_id' AND b.date ='$date'";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }
    
  function booked_seat($timing_id,$date){


    // $sql = "SELECT * FROM `booking` WHERE timing_id=$timing_id AND date=$date";
    $attr = array('timing_id' => $timing_id,'date' => $date);
    $this->db->select('booking.seat_no');
    $this->db->from('booking');
    $this->db->where($attr);
    $query = $this->db->get();
    return $query->result();

  }
  



function check_route($from,$to){
        $this->db->select('id');
       // $this->db->select('CONCAT(l1.title,'-',l2.title) as route_name');
       // $this->db->join(' location l1 on l1.id=route.bus_from, location l2 on l2.id = route.bus_to ');
       $this->db->from('route');
       $this->db->where(array(
        'bus_from'=>$from,
        'bus_to'=>$to,
      ));
        $query = $this->db->get();
        // echo "<pre>";
       //  print_r($query->result());
        // echo "";
      if($query->num_rows()>0){
        $result = $query->result_array();
        return $result[0]['id'];
       }else{
      return false;
       }
    }
   function get_timinginfo_by_id($timing_id){
    
        $this->db->select('timing.*,bus_type.title,bus_type.seat,bus_type.amount,btime.time');
        $this->db->from('timing');
        $this->db->join('btime','btime.id=timing.btime_id',"left");
        $this->db->join('bus_type','bus_type.id=timing.bus_type_id',"left");
        //$this->db->where('timing.route_id',$route_id);
        $this->db->where('timing.id',$timing_id);
      $query = $this->db->get();
        if($query->num_rows()>0) {
            return $query->result_array();
        }else{
            return false;
        }
      }

     function get_timinginfo($route_id){

        $this->db->select('timing.*,bus_info.bus_name,bus_info.bus_rnumbr,bus_type.title,bus_type.seat,bus_type.amount,btime.time');
        $this->db->from('timing');
        $this->db->join('btime','btime.id=timing.btime_id',"left");
        $this->db->join('bus_info','bus_info.id=timing.bus_info_id',"left");
        
        $this->db->join('bus_type','bus_type.id=timing.bus_type_id',"left");
        //$this->db->where('timing.id',$route_id);
          $this->db->where('timing.route_id',$route_id);
       // $this->db->where('timing.id',$timming_id);

      $query = $this->db->get();
        if($query->num_rows()>0) {
            return $query->result_array();
        }else{
            return false;
        }

    }  
  
 

 //function get_bookinginfo_id(){

 //$s//ql="      
     // SELECT timing.id,timing.route_id,route.bus_from,route.bus_to, btime.time, bus_type.title,bus_type.seat,//bus_type.amount,booking.date

     //FROM `timing` left join route on route.id =timing.route_id left join btime on btime.id = timing.btime_id left join bus_type on bus_type.id = timing.bus_type_id left JOIN booking ON booking.id=timing.booking_id 
     //    WHERE timing_id = timing_id";
     // $query = $this->db->get();
      //  if($query->num_rows()>0) {
       //     return $query->result_array();
      //  }else
      //      return false;
      //  }

      
  
 







 //function get_timinginfo(){
//$this->db->select('*');
//$this->db->select('CONCAT(l1.title,'-',l2.title) as route_name');
//$this->db->from(' route');
 //$this->db->where(array(
   //     'route_id'=>$btime_id,
       
    //  ));
  //$query = $this->db->get();
  //if($query->num_rows()>0){
      //  $result = $query->result_array();
        //print_r($result);
       // return $result[0]['id'];
       //}else{
      //return false;
       
   // }
   

 function get_routeinfo(){
        $sql = "SELECT route.id,l1.title as bus_from_title,l2.title as bus_to_title FROM `route` left JOIN location l1 ON l1.id = route.bus_from left join location l2 ON l2.id = route.bus_to";
      $query = $this->db->query($sql);
      return $query->result();
      
    }

   
       function get_btimeinfo(){
        $query = $this->db->get('btime');
        return $query->result();

}
 function get_locationinfo(){
        $query = $this->db->get('location');
        return $query->result();
    }


public function StoreQueryData($QueryData){

  $this->db->insert('query_table',$QueryData);
  return true;
}

 function get_queryinfo(){
        $query = $this->db->get('query_table');
        return $query->result();
    }



function delete_query($q_id){
      $where =array(
        "id" => $q_id
      );
      
      $query = $this->db->delete('query_table',$where); 
      if($query){
        return true;
      }else{
        return false;
      }
      }

function get_customer_info($id){

    $attr = array('login.id' => $id);
    $this->db->select('*');
    $this->db->from('customer');
    $this->db->join('login','customer.login_id = login.id');
    $this->db->where($attr);
    $query = $this->db->get();
    return $query->result();
}
// function get_j_info($id){

//     $attr = array('customer.login_id = booking.id');
//     $this->db->select('*');
//     $this->db->from('booking');
//     //$this->db->join('login','customer.login_id = login.id');
//     $this->db->where($attr);
//     $query = $this->db->get();
//     return $query->result();
// }
function booking_code($cusData){
// echo 'lol';
// exit();
  $userId = $this->session->userdata('login_id');
  $route_id = $cusData['route_id'];
  $doj = $cusData['doj'];
  $timing_id = $cusData['timing_id'];
  $bus_fare = $cusData['bus_fare'];
  $seatNo = $cusData['seatNo'];
  $indexNo = $cusData['indexNo'];
  $busType = $cusData['busType'];

  $i = 0;
  foreach ($seatNo as $seat) {
    $insertData = array(
      'date' => $doj,
      'user_id' => $userId,
      'timing_id' => $timing_id,
      'route_id' => $route_id,
      'bus_type_id' => $busType[$i],
      'seat_no' => $seat,
      'index_no' => $indexNo[$i],
      'fare' => $bus_fare,
    );
    $this->db->insert('booking',$insertData);
    $i++;
  }

  return true;

}

}













