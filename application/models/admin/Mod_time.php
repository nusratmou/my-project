<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for time controller
*@param 
*@return 
*@author 
*@version 1.0 (10-27-2018)
*/
class Mod_time extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_timeinfo(){
        $sql="SELECT timing.id as timing_id,route.id, btime.time as bus_tym,bus_type.title as bus_typeL, CONCAT(l1.title,'-',l2.title) as route_name FROM `timing` left join route on route.id = timing.route_id
        left join btime on btime.id = timing.bus_time left join bus_type on bus_type.id=timing.bus_type_id left join location l1 on l1.id=route.bus_from left join location l2 on l2.id = route.bus_to";
    	$query = $this->db->query($sql);
        return $query->result();
        
    	

    
    }
      function get_routeinfo(){
        $query = $this->db->query('SELECT route.id,CONCAT(l1.title,"-", l2.title) as route_name FROM `route` left join location l1 on l1.id = route.bus_from left JOIN location l2 on l2.id = route.bus_to');
        return $query->result();
    }

        function get_bus_typeinfo(){
        $query = $this->db->get('bus_type');
        return $query->result();

}

        function get_btimeinfo(){
        $query = $this->db->get('btime');
        return $query->result();

}

 


    /**
    *
    *Description : This method for getting info of individual time by     *their id.
    *@param $b_id time Id
    *@return $result time info
    *@author 
    *@version 1.0 (10-27-2018)
    */

    function get_timeinfo_by_id($timing_id){
    	$where =array(
    		"id" => $timing_id
    	);
    	$query = $this->db->get_where('timing',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting time info
    *@param $PostData array time data
    *@return $result True/False
    *@author 
    *@version 1.0 (11-04-2018)
    */

    function insert_time($postData){
    	
     $datas = array(
            
             'route_id' => $postData['route_name'],
           'bus_type_id' => $postData['title'],
            'bus_time' => $postData['time'],
           
          

        );
    	$query = $this->db->insert('timing',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng time info
    *@param $PostData array time data
    *@param $time ID Int time unique id
    *@return $result True/False
    *@author 
    *@version 1.0 (11-04-2018)
    */

    function update_time($postData,$timing_id){
    	$where =array(
    		"id" => $timing_id
    	);
    	 $datas = array(
              
            
             'route_id' => $postData['route_name'],
           'bus_type_id' => $postData['title'],
            'bus_time' => $postData['time'],
  

        );
    	$query = $this->db->update('timing',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting time info
    *@param $PostData array time data
    *@param $time ID Int time unique id
    *@return $result True/False
    *@author 
    *@version 1.0 (11-04-2018)
    */

   
    function delete_timeinfo($id){
        $where =array(
            "id" => $id
        );
        
        $query = $this->db->delete('timing',$where); 
        if($query){
            return true;
        }else{
            return false;
        }
        
    }


}
