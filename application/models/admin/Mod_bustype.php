<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for bus controller
*@param 
*@return 
*@author 
*@version 1.0 (10-27-2018)
*/
class Mod_bustype extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }
    
     function get_bus_typeinfo(){
        $sql="SELECT bus_type.id as bus_type_id, bus_type.title as title, bus_type.seat as seat, bus_type.layout as layout, bus_type.snumber as snumber, bus_type.amount as amount,btime.time as bus_tym,route.id, CONCAT(l1.title,'-',l2.title) as route_name FROM `bus_type` left join route on route.id = bus_type.route_id left join location l1 on l1.id=route.bus_from left join location l2 on l2.id = route.bus_to  left join btime on btime.id = bus_type.bus_time";
        $query = $this->db->query($sql);
       return $query->result();

    }
         

  function get_routeinfo(){
        $query = $this->db->query('SELECT route.id,CONCAT(l1.title,"-", l2.title) as route_name FROM `route` left join location l1 on l1.id = route.bus_from left JOIN location l2 on l2.id = route.bus_to');
        return $query->result();
    }
 function get_btimeinfo(){
        $query = $this->db->get('btime');
        return $query->result();

}
//function get_bus_typeinfo(){
//      $query = $this->db->get('bus_type');
 //       return $query->result();
//}

    /**
    *
    *Description : This method for getting info of individual bus by     *their id.
    *@param $b_id bus Id
    *@return $result bus info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_bustypeinfo_by_id($id){
    	$where =array(
    		"id" => $id
    	);
    	$query = $this->db->get_where('bus_type',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting bus info
    *@param $PostData array bus data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_bustype($postData){
    	
     $datas = array(
           'route_id' => $postData['route_name'],
           'title' => $postData['title'],
           'seat' => $postData['seat'],
            'layout' => $postData['layout'],
           'snumber' => $postData['snumber'],
            'bus_time' => $postData['time'],
           'amount'  => $postData['amount']

        );
    	$query = $this->db->insert('bus_type',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng bus info
    *@param $PostData array bus data
    *@param $bus ID Int bus unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_bustype($postData,$id){
    	$where =array(
    		"id" => $id
    	);
    	 $datas = array(
            'route_id' => $postData['route_name'],
            'title' => $postData['title'],
           'seat' => $postData['seat'],
            'layout' => $postData['layout'],
           'snumber' => $postData['snumber'],
            'bus_time' => $postData['time'],
             'amount'  => $postData['amount']
        );
    	$query = $this->db->update('bus_type',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting bus info
    *@param $PostData array bus data
    *@param $bus ID Int bus unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_bus_typeinfo($bus_type_id){
    	$where =array(
    		"id" => $id
    	);
    	
    	$query = $this->db->delete('bus_type',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}
  