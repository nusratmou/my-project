<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for home controller
*@param 
*@return 
*@author Tj Thouhid
*@version 1.0 (10-27-2018)
*/
class Mod_booking extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_bookinginfo(){
    	$query = $this->db->get('bus_booking');
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for getting info of individual booking by     *their id.
    *@param $s_id booking Id
    *@return $result booking info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_bookinginfo_by_id($booking_id){
    	$where =array(
    		"id" => $booking_id
    	);
    	$query = $this->db->get_where('bus_booking',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting booking info
    *@param $PostData array booking data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_booking($postData){
    	
    	$datas = array(
    		'b_name' => $postData['fullname'],
    		'b_email' => $postData['email'],
            'b_pass' => $postData['password'],
    		'b_route' => $postData['route'],
    		'b_seat' => $postData['seat'],
    	    'b_amount' => $postData['amount'],
            'b_bustype' => $postData['bustype'],
            'date' => $postData['date']
    		
    	);
    	$query = $this->db->insert('bus_booking',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng booking info
    *@param $PostData array booking data
    *@param $booking ID Int booking unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_booking($postData,$booking_id){
    	$where =array(
    		"id" => $booking_id
    	);
    	$datas = array(
    		  'b_name' => $postData['fullname'],
            'b_email' => $postData['email'],
            'b_pass' => $postData['password'],
            'b_route' => $postData['route'],
            'b_seat' => $postData['seat'],
            'b_amount' => $postData['amount'],
            'b_bustype' => $postData['bustype'],
            'date' => $postData['date']
    	);
    	$query = $this->db->update('bus_booking',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting booking info
    *@param $PostData array booking data
    *@param $booking ID Int booking unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_booking($booking_id){
    	$where =array(
    		"id" => $booking_id
    	);
    	
    	$query = $this->db->delete('bus_booking',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    