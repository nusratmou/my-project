<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Mod_reserve extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

 
   function get_bookinginfo(){
         $sql=" SELECT booking.id,l1.title as bus_type_id,b1.seat as bus_type_id,booking.origin as origin,booking.destination as destination,booking.name as name FROM `booking` left JOIN bus_type l1 ON l1.id = booking.bus_type_id left join bus_type b1 ON b1.id = booking.bus_type_id";
        $query = $this->db->query($sql);
        return $query->result();
        
    }

 function get_bus_typeinfo(){
        $query = $this->db->get('bus_type');
        return $query->result();

}

}
