<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for agent controller
*@param 
*@return 
*@author 
*@version 1.0 (10-27-2018)
*/
class Mod_agent extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_agentinfo(){
    	$query = $this->db->get('bus_agent');
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for getting info of individual agent by     *their id.
    *@param $s_id agent Id
    *@return $result agent info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_agentinfo_by_id($agent_id){
    	$where =array(
    		"id" => $agent_id
    	);
    	$query = $this->db->get_where('bus_agent',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting agent info
    *@param $PostData array agent data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_agent($postData){
    	
    	$datas = array(
            'b_name' => $postData['fullname'],
            'b_email' => $postData['email'],
             'b_pass' => md5($postData['password']),
           'phone' => $postData['phone'],
            'b_cname' => $postData['cname'],
            'b_address' => $postData['address'],
            'b_city' => $postData['city']
           
    	);
    	$query = $this->db->insert('bus_agent',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng agent info
    *@param $PostData array agent data
    *@param $agent ID Int agent unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_agent($postData,$agent_id){
    	$where =array(
    		"id" => $agent_id
    	);
    	$datas = array(
            'b_name' => $postData['fullname'],
            'b_email' => $postData['email'],
             'b_pass' => md5($postData['password']),
           'phone' => $postData['phone'],
            'b_cname' => $postData['cname'],
            'b_address' => $postData['address'],
            'b_city' => $postData['city'],
           
        );
    	$query = $this->db->update('bus_agent',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting agent info
    *@param $PostData array agent data
    *@param $agent ID Int agent unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_agent($agent_id){
    	$where =array(
    		"id" => $agent_id
    	);
    	
    	$query = $this->db->delete('bus_agent',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    