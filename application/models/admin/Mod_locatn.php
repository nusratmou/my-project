<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for locatn controller
*@param 
*@return 
*@author 
*@version 1.0 (10-27-2018)
*/
class Mod_locatn extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_locatninfo(){
    	$query = $this->db->get('location');
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for getting info of individual locatn by     *their id.
    *@param $b_id locatn Id
    *@return $result locatn info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_locatninfo_by_id($id){
    	$where =array(
    		"id" => $id
    	);
    	$query = $this->db->get_where('location',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting locatn info
    *@param $PostData array locatn data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_locatn($postData){
    	
     $datas =array(
         'title' => $postData['title'],
          

        );
    	$query = $this->db->insert('location',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng locatn info
    *@param $PostData array locatn data
    *@param $locatn ID Int locatn unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_locatn($postData,$id){
    	$where =array(
    		"id" => $id
    	);
    	 $datas = array(
            'title' => $postData['title'],
          
        );
    	$query = $this->db->update('location',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting locatn info
    *@param $PostData array locatn data
    *@param $locatn ID Int locatn unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_locatn($id){
    	$where =array(
    		"id" => $id
    	);
    	
    	$query = $this->db->delete('location',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    