<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for bus controller
*@param 
*@return 
*@author 
*@version 1.0 (10-27-2018)
*/
class Mod_cposition extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }
    
    function get_cpositioninfo(){
       $sql=" SELECT current_position.id,l1.title as location_id,b1.bus_name as bus_info_id FROM `current_position` left JOIN location l1 ON l1.id = current_position.location_id left join bus_info b1 ON b1.id = current_position.bus_info_id";
        $query = $this->db->query($sql);
        return $query->result();
        
        
    }

    function get_businfo(){
    	$query = $this->db->get('bus_info');
    	return $query->result();
    	
    }
    function get_locationinfo(){
        $query = $this->db->get('location');
        return $query->result();
    }

    /**
    *
    *Description : This method for getting info of individual bus by     *their id.
    *@param $b_id bus Id
    *@return $result bus info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_cpositioninfo_by_id($id){
    	$where =array(
    		"id" => $id
    	);
    	$query = $this->db->get_where('current_position',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting bus info
    *@param $PostData array bus data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_cposition($postData){
    	
     $datas = array(
            'location_id' => $postData['location'],
           'bus_info_id' => $postData['bname'],
            
          

        );
    	$query = $this->db->insert('current_position',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /*
    *
    *Description : This method for Updatng bus info
    *@param $PostData array bus data
    *@param $bus ID Int bus unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_cposition($postData,$id){
    	$where =array(
    		"id" => $id
    	);
    	 $datas = array(
             'location_id' => $postData['location'],
           'bus_info_id' => $postData['bname'],
            
        );
    	$query = $this->db->update('current_position',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting bus info
    *@param $PostData array bus data
    *@param $bus ID Int bus unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_cpositioninfo($id){
    	$where =array(
    		"id" => $id
    	);
    	
    	$query = $this->db->delete('current_position',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    