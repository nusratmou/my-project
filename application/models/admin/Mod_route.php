<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*Description : This model is for home controller
*@param 
*@return 
*@author Tj Thouhid
*@version 1.0 (10-27-2018)
*/
class Mod_route extends CI_Model {
 
 

 
    public function __construct()
    {
        parent::__construct();
        

    }

    function get_routeinfo(){
        $sql = "SELECT route.id,l1.title as bus_from_title,l2.title as bus_to_title FROM `route` left JOIN location l1 ON l1.id = route.bus_from left join location l2 ON l2.id = route.bus_to";
    	$query = $this->db->query($sql);
    	return $query->result();
    	
    }

    function get_locationinfo(){
        $query = $this->db->get('location');
        return $query->result();
    }


    /**
    *
    *Description : This method for getting info of individual route by     *their id.
    *@param $route_id route Id
    *@return $result route info
    *@author Tj Thouhid
    *@version 1.0 (10-27-2018)
    */

    function get_routeinfo_by_id($route_id){
    	$where =array(
    		"id" => $route_id
    	);
    	$query = $this->db->get_where('route',$where);
    	return $query->result();
    	
    }


    /**
    *
    *Description : This method for Inserting route info
    *@param $PostData array route data
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function insert_route($postData){
    	
    $datas = array(
            'bus_from' => $postData['bfrom'],
             'bus_to' => $postData['bto']
            

        );
    	$query = $this->db->insert('route',$datas );
    	if($query){
    		return $insert_id = $this->db->insert_id();
    		
    	}else{
    		return false;
    	}
    	
    }
    /**
    *
    *Description : This method for Updatng route info
    *@param $PostData array route data
    *@param $route ID Int route unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function update_route($postData,$route_id){
    	$where =array(
    		"id" => $route_id
    	);
    	$datas = array(
            'bus_from' => $postData['bfrom'],
             'bus_to' => $postData['bto']

    	);
    	$query = $this->db->update('route',$datas ,$where);
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    } 



    /**
    *
    *Description : This method for Deleting route info
    *@param $PostData array route data
    *@param $route ID Int route unique id
    *@return $result True/False
    *@author Tj Thouhid
    *@version 1.0 (11-04-2018)
    */

    function delete_route($route_id){
    	$where =array(
    		"id" => $route_id
    	);
    	
    	$query = $this->db->delete('route',$where); 
    	if($query){
    		return true;
    	}else{
    		return false;
    	}
    	
    }


}

    